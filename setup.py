#!/usr/bin/python2.7
# coding=utf-8

from setuptools import setup, find_packages

if __name__ == "__main__":

    setup(
        name = "VhostManager",
        version = "0.2.0",
        author = "Antton Hytönen",
        author_email = "hytonen-a@hotmail.com",

        packages = [

            find_packages("vhmadmin"),
            find_packages("vhmapi"),
            find_packages("vhmdb"),
            find_packages("vhmmanager"),
            find_packages("vhmmisc"),
            find_packages("vhmstudent"),
            find_packages("vhmteacher"),

            find_packages("limyapi"),

        ],
        scripts = ["bin/vhmmanage"],
        data_files = [
            ("/var/vhostmgr/templates", [
                "default_templates/interfaces",
                "default_templates/vhost_info.html",
                "default_templates/vhosts",
            ]),
        ],

        url = "http://bitbucket.org/Winded/vhostmgr",
        license = "LICENSE.txt",
        description = "A rental system of Apache VirtualHosts",
        long_description = open("README.md").read(),
        requires=['django', 'south', 'djangorestframework', 'django-rosetta', 'mysql-python', 'python-dateutil'],
    )