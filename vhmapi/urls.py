from django.conf.urls import patterns, url, include
from vhmapi.student import views


urlpatterns = patterns('',

    url(r"^student/", include("vhmapi.student.urls")),
    url(r"^admin/", include("vhmapi.admin.urls")),
    url(r"^shared/", include("vhmapi.shared.urls")),

)