from dateutil.parser import parse
from django.utils.translation import ugettext
from rest_framework.generics import GenericAPIView
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND
from vhmapi.shared.serializers import UserSerializer, VhostSerializer
from vhmapi.student.views import LoginAPIMixin
from vhmdb.models.sitepackage import SitePackage
from vhmdb.models.vhost import Vhost


class OptionMixin(object):

    def get_queryset(self):
        raise NotImplementedError()

    def list_option(self, request, *args, **kwargs):
        qs = self.get_queryset()
        objects = []
        for obj in qs:
            objs = {
                "label": str(obj),
                "value": obj.pk,
            }
            objects.append(objs)
        return Response(objects)


class UserView(LoginAPIMixin):

    def get(self, request, *args, **kwargs):
        serializer = UserSerializer(request.vhm_user)
        return Response(serializer.data)


class UserChangePasswordView(LoginAPIMixin):
    def post(self, request, *args, **kwargs):
        user = request.vhm_user

        password = request.DATA.get("password")
        if not password:
            return Response({"detail": "Invalid or no password given"}, status=HTTP_400_BAD_REQUEST)

        user.set_password(password)
        user.save()

        return Response({"success": True})


class VhostOptionsView(GenericAPIView, LoginAPIMixin, OptionMixin):

    def get_queryset(self):
        return Vhost.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list_option(request, *args, **kwargs)


class VhostAvailableView(LoginAPIMixin):
    def get(self, request, *args, **kwargs):
        start_date = request.QUERY_PARAMS.get("start_date", None)
        end_date = request.QUERY_PARAMS.get("end_date", None)
        try:
            start_date = parse(start_date)
            end_date = parse(end_date)
        except:
            return Response({"detail": "Failed to parse start or end date."}, status=HTTP_400_BAD_REQUEST)

        vhost = request.QUERY_PARAMS.get("vhost")

        if not vhost:
            vhost = Vhost.get_available_at(start_date, end_date)
            if vhost is None:
                return Response({"detail": ugettext("No VirtualHosts available for the given time.")},
                                status=HTTP_404_NOT_FOUND)
            serializer = VhostSerializer(vhost)
            return Response(serializer.data)
        else:
            try:
                vhost = int(vhost)
                vhost = Vhost.objects.get(pk=vhost)
            except (ValueError, Vhost.DoesNotExist):
                return Response({"detail": "VirtualHost does not exist."}, status=HTTP_400_BAD_REQUEST)

            available = not vhost.is_rented_at(start_date, end_date)
            return Response({"available": available})


class SitePackageOptionsView(GenericAPIView, LoginAPIMixin, OptionMixin):

    def get_queryset(self):
        return SitePackage.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list_option(request, *args, **kwargs)