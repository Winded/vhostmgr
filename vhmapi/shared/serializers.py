from rest_framework.fields import Field, SerializerMethodField
from rest_framework.relations import PrimaryKeyRelatedField
from rest_framework.serializers import ModelSerializer
from vhmdb.models.rent import VhostRent
from vhmdb.models.user import User
from vhmdb.models.vhost import Vhost


class UserSerializer(ModelSerializer):

    max_vhosts_display = Field(source="get_max_vhosts_display")

    status_display = Field(source="get_status_display")

    date_created_display = Field(source="get_date_created_display")

    active_rents = SerializerMethodField("get_active_rents")

    class Meta:
        model = User
        exclude = ("password",)

    def get_active_rents(self, obj):
        return obj.rents.filter(status=VhostRent.ACTIVE).count()


class VhostSerializer(ModelSerializer):

    interface = PrimaryKeyRelatedField(required=False)
    # interface_obj

    endpoint = Field(source="get_address_port")

    options_display = Field(source="options")

    is_active = Field(source="is_active")
    is_active_display = Field(source="get_is_active_display")

    class Meta:
        model = Vhost
        depth = 1