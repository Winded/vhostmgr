from django.conf.urls import patterns, url
from vhmapi.shared.views import UserView, UserChangePasswordView, VhostOptionsView, SitePackageOptionsView, \
    VhostAvailableView


urlpatterns = patterns('',

    url(r"^user/$", UserView.as_view(), name="vhmapi_student_user"),
    url(r"^user/set_password/$", UserChangePasswordView.as_view(), name="vhmapi_student_user_set_password"),

    url(r"^vhosts/options/$", VhostOptionsView.as_view(), name="vhmapi_student_vhost_options"),
    url(r"^vhosts/get_available/$", VhostAvailableView.as_view(), name="vhmapi_student_vhost_available"),

    url(r"^packages/options/$", SitePackageOptionsView.as_view(), name="vhmapi_student_package_options"),

)