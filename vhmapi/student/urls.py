from django.conf.urls import patterns, url
from vhmapi.shared.views import UserView, UserChangePasswordView, VhostOptionsView, SitePackageOptionsView, \
    VhostAvailableView
from vhmapi.student import views


urlpatterns = patterns('',

    url(r"^user/$", UserView.as_view(), name="vhmapi_student_user"),
    url(r"^user/set_password/$", UserChangePasswordView.as_view(), name="vhmapi_student_user_set_password"),

    url(r"^vhosts/$", views.VhostAllView.as_view(), name="vhmapi_student_vhost_all"),
    url(r"^vhosts/(?P<pk>\d+)/$", views.VhostOneView.as_view(), name="vhmapi_student_vhost_one"),

    url(r"^rents/$", views.VhostRentAllView.as_view(), name="vhmapi_student_rent_all"),
    url(r"^rents/(?P<pk>\d+)/$", views.VhostRentOneView.as_view(), name="vhmapi_student_rent_one"),
    url(r"^rents/(?P<pk>\d+)/cancel/$", views.VhostRentCancelView.as_view(), name="vhmapi_student_rent_cancel"),

    url(r"^packages/$", views.SitePackageAllView.as_view(), name="vhmapi_student_package_all"),
    url(r"^packages/(?P<pk>\d+)/$", views.SitePackageOneView.as_view(), name="vhmapi_student_package_one"),

)