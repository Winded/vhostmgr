from django.template.defaultfilters import filesizeformat
from rest_framework.fields import Field, SerializerMethodField
from rest_framework.relations import PrimaryKeyRelatedField, HyperlinkedRelatedField
from rest_framework.serializers import ModelSerializer, BaseSerializer
from vhmdb.models.rent import VhostRent
from vhmdb.models.sitepackage import SitePackage
from vhmdb.models.user import User
from vhmdb.models.vhost import Vhost
from vhmmisc.requests import get_request


class PreSaveMixin(BaseSerializer):

    def process_fields(self, attrs):
        return attrs

    def restore_fields(self, data, files):
        rdata = super(PreSaveMixin, self).restore_fields(data, files)
        rdata = self.process_fields(rdata)
        return rdata


class SitePackageSerializer(ModelSerializer):

    size = Field(source="package.size")
    size_display = SerializerMethodField("get_size_display")

    class Meta:
        model = SitePackage
        exclude = ("type", "package")

    def get_size_display(self, obj):
        return filesizeformat(obj.package.size)


class VhostRentSerializer(ModelSerializer, PreSaveMixin):

    vhost = PrimaryKeyRelatedField()
    vhost_display = Field(source="vhost")
    package = PrimaryKeyRelatedField(required=False)
    package_display = Field(source="package")

    start_date_display = Field(source="get_start_date_display")
    end_date_display = Field(source="get_end_date_display")

    status_display = Field(source="get_status_display")

    class Meta:
        model = VhostRent
        exclude = ("user",)
        read_only_fields = ("status",)
        depth = 1

    def process_fields(self, attrs):
        if not "user" in attrs:
            attrs["user"] = get_request().vhm_user
        return attrs