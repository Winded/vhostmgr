from dateutil.parser import parse
from django.http.response import HttpResponseForbidden
from django.utils.translation import ugettext
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_404_NOT_FOUND
from rest_framework.views import APIView
from vhmapi.shared.serializers import VhostSerializer
from vhmdb.models.rent import VhostRent
from vhmdb.models.sitepackage import SitePackage
from vhmdb.models.user import User
from vhmdb.models.vhost import Vhost
from vhmapi.student.serializers import VhostRentSerializer, SitePackageSerializer
from vhmmisc.requests import get_request
from vhmweb import settings


class LoginAPIMixin(APIView):

    def dispatch(self, request, *args, **kwargs):

        if not isinstance(request.vhm_user, User):
            return HttpResponseForbidden()

        return super(LoginAPIMixin, self).dispatch(request, *args, **kwargs)


class VhostAllView(GenericAPIView, LoginAPIMixin, ListModelMixin):

    serializer_class = VhostSerializer

    def get_queryset(self):
        return Vhost.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class VhostOneView(GenericAPIView, LoginAPIMixin, RetrieveModelMixin):

    serializer_class = VhostSerializer

    def get_queryset(self):
        return Vhost.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class VhostRentAllView(GenericAPIView, LoginAPIMixin, ListModelMixin, CreateModelMixin):

    serializer_class = VhostRentSerializer

    def get_queryset(self):
        return VhostRent.objects.filter(user=get_request().vhm_user)

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def pre_save(self, obj):
        if settings.VHOST_AUTO_APPROVE or obj.user.is_admin():
            obj.status = VhostRent.WAITING


class VhostRentOneView(GenericAPIView, LoginAPIMixin, RetrieveModelMixin):

    serializer_class = VhostRentSerializer

    def get_queryset(self):
        return VhostRent.objects.filter(user=get_request().vhm_user)

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class VhostRentCancelView(GenericAPIView, LoginAPIMixin):

    serializer_class = VhostRentSerializer

    def get_queryset(self):
        return VhostRent.objects.filter(user=get_request().vhm_user, status__in=VhostRent.STATUS_CURRENT)

    def post(self, request, *args, **kwargs):
        obj = self.get_object()
        obj.status = VhostRent.PASSED
        obj.save()
        s = self.get_serializer(obj)
        return Response(s.data)


class SitePackageAllView(GenericAPIView, LoginAPIMixin, ListModelMixin):

    serializer_class = SitePackageSerializer

    def get_queryset(self):
        return SitePackage.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class SitePackageOneView(GenericAPIView, LoginAPIMixin, RetrieveModelMixin):

    serializer_class = SitePackageSerializer

    def get_queryset(self):
        return SitePackage.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)