import json
from datetime import timedelta
from django.core.urlresolvers import reverse
from django.test.client import Client
from django.test.testcases import TestCase
from django.utils import timezone
from rest_framework.test import APIRequestFactory
from vhmdb.models.rent import VhostRent
from vhmdb.models.user import User
from vhmdb.models.vhost import Vhost


class APITests(TestCase):

    def setUp(self):
        u = User(name="john", display_name="John Doe")
        u.set_password("abc123")
        u.save()
        vh = Vhost.objects.create(name="bumblr", port=1234)
        self.assertIsNotNone(vh.meta)
        c = Client()
        r = c.post(reverse("vhmstudent_login"), {"username": "john", "password": "abc123", "next": "/"})
        self.assertLess(r.status_code, 400)
        self.client = c

    def test_rent_creation(self):
        r = self.client.post(reverse("vhmapi_student_rent_all"), {
            "vhost": Vhost.objects.get(name="bumblr").pk,
            "package": None,
            "start_date": "2015-03-24T21:10:00Z",
            "end_date": "2015-03-24T22:10:00Z"
        })
        self.assertLess(r.status_code, 400)
        resp = json.loads(r.content)
        self.assertEqual(resp["status"], VhostRent.PENDING)

    def test_rent_cancel(self):
        rent = VhostRent.objects.create(
            user=User.objects.get(name="john"),
            vhost=Vhost.objects.get(name="bumblr"),
            start_date=timezone.now() + timedelta(hours=1),
            end_date=timezone.now() + timedelta(hours=2),
            status=VhostRent.WAITING,
        )
        r = self.client.post(reverse("vhmapi_student_rent_cancel", kwargs={"pk": rent.pk}))
        self.assertLess(r.status_code, 400)
        rent = VhostRent.objects.get(pk=rent.pk)
        self.assertEqual(rent.status, VhostRent.PASSED)


class APIAdminTests(TestCase):

    def setUp(self):
        u = User(name="admin", display_name="Admin", status=User.ADMIN)
        u.set_password("supersecret")
        u.save()
        Vhost.objects.create(name="bumblr", port=1234)
        c = Client()
        r = c.post(reverse("vhmstudent_login"), {"username": "admin", "password": "supersecret", "next": "/"})
        self.assertLess(r.status_code, 400)
        self.client = c

    def test_rent_creation(self):
        r = self.client.post(reverse("vhmapi_student_rent_all"), {
            "vhost": Vhost.objects.get(name="bumblr").pk,
            "package": None,
            "start_date": "2015-03-24T21:10:00Z",
            "end_date": "2015-03-24T22:10:00Z"
        })
        self.assertLess(r.status_code, 400)
        resp = json.loads(r.content)
        self.assertEqual(resp["status"], VhostRent.WAITING)