from django.contrib.auth import hashers
from rest_framework.fields import Field, CharField, DateTimeField
from rest_framework.relations import PrimaryKeyRelatedField, HyperlinkedRelatedField, HyperlinkedIdentityField
from rest_framework.serializers import ModelSerializer, Serializer
from vhmapi.shared.serializers import VhostSerializer as SharedVhostSerializer
from vhmapi.student import serializers as student
from vhmapi.student.serializers import PreSaveMixin
from vhmdb.models.sitepackage import SitePackage
from vhmdb.models.user import User, UserGroup


class UserSerializer(ModelSerializer, PreSaveMixin):

    max_vhosts_display = Field(source="get_max_vhosts_display")

    status_display = Field(source="get_status_display")

    date_created_display = Field(source="get_date_created_display")

    class Meta:
        model = User
        exclude = ("password",)

    def process_fields(self, attrs):
        return attrs


class UserPasswordSerializer(Serializer):

    password = CharField(required=True)


class UserGroupSerializer(ModelSerializer):

    class Meta:
        model = UserGroup


class VhostSerializer(SharedVhostSerializer):

    class Meta(SharedVhostSerializer.Meta):
        pass


class SitePackageSerializer(student.SitePackageSerializer):

    type_display = Field(source="get_type_display")

    class Meta(student.SitePackageSerializer.Meta):
        exclude = ("package",)
        read_only_fields = ("type",)


class VhostRentSerializer(student.VhostRentSerializer):

    user = PrimaryKeyRelatedField()
    user_display = Field(source="user.display_name")

    class Meta(student.VhostRentSerializer.Meta):
        exclude = ()
        read_only_fields = ()

    def process_fields(self, attrs):
        return attrs


class VhostRentGroupSerializer(Serializer):

    group = PrimaryKeyRelatedField(queryset=UserGroup.objects.all())

    start_date = DateTimeField()
    end_date = DateTimeField()

    package = PrimaryKeyRelatedField(queryset=SitePackage.objects.all(), null=True)