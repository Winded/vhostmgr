from django.conf.urls import patterns, url
from vhmapi.admin import views
from vhmapi.shared.views import UserView, UserChangePasswordView, VhostOptionsView, SitePackageOptionsView, \
    VhostAvailableView


urlpatterns = patterns('',

    url(r"^user/$", UserView.as_view(), name="vhmapi_admin_user"),
    url(r"^user/set_password/$", UserChangePasswordView.as_view(), name="vhmapi_admin_user_set_password"),

    url(r"^users/$", views.UserAllView.as_view(), name="vhmapi_admin_user_all"),
    url(r"^users/options/$", views.UserOptionsView.as_view(), name="vhmapi_admin_user_options"),
    url(r"^users/(?P<pk>\d+)/$", views.UserOneView.as_view(), name="vhmapi_admin_user_one"),
    url(r"^users/(?P<pk>\d+)/change_password/$", views.UserOnePasswordView.as_view(), name="vhmapi_admin_user_one_password"),

    url(r"^groups/$", views.UserGroupAllView.as_view(), name="vhmapi_admin_group_all"),
    url(r"^groups/options/$", views.UserGroupOptionsView.as_view(), name="vhmapi_admin_group_options"),
    url(r"^groups/(?P<pk>\d+)/$", views.UserGroupOneView.as_view(), name="vhmapi_admin_group_one"),

    url(r"^rents/$", views.VhostRentAllView.as_view(), name="vhmapi_admin_rent_all"),
    url(r"^rents/group/$", views.VhostRentForGroupView.as_view(), name="vhmapi_admin_rent_for_group"),
    url(r"^rents/(?P<pk>\d+)/$", views.VhostRentOneView.as_view(), name="vhmapi_admin_rent_one"),
    url(r"^rents/(?P<pk>\d+)/cancel/$", views.VhostRentCancelView.as_view(), name="vhmapi_admin_rent_cancel"),

    url(r"^vhosts/$", views.VhostAllView.as_view(), name="vhmapi_admin_vhost_all"),
    url(r"^vhosts/(?P<pk>\d+)/$", views.VhostOneView.as_view(), name="vhmapi_admin_vhost_one"),

    url(r"^packages/$", views.SitePackageAllView.as_view(), name="vhmapi_admin_package_all"),
    url(r"^packages/(?P<pk>\d+)/$", views.SitePackageOneView.as_view(), name="vhmapi_admin_package_one"),

)