import StringIO
import json
from datetime import timedelta
import os
import zipfile
from django.conf import settings
from django.contrib.auth import hashers
from django.core.urlresolvers import reverse
from django.db.backends.mysql.base import adapt_datetime_with_timezone_support
from django.test.client import Client
from django.test.testcases import TestCase
from django.utils import timezone
from rest_framework.test import APIRequestFactory
from vhmdb.models.rent import VhostRent
from vhmdb.models.sitepackage import SitePackage
from vhmdb.models.user import User
from vhmdb.models.vhost import Vhost


class APIAdminTests(TestCase):

    def setUp(self):
        u = User(name="admin", display_name="Admin", status=User.ADMIN)
        u.set_password("supersecret")
        u.save()
        u = User(name="test", display_name="Test user", max_vhosts=2, status=User.STUDENT)
        u.set_password("abc123")
        u.save()
        vh = Vhost.objects.create(name="bumblr", port=1234)
        sp = SitePackage.objects.create(name="wordpress", description="Words words words", type=SitePackage.ZIP,
                                        package="files/package1.zip")
        VhostRent.objects.create(user=u, vhost=vh, package=sp, status="P",
                                 start_date=timezone.make_aware(timezone.datetime(2015, 3, 24, 18), timezone.utc),
                                 end_date=timezone.make_aware(timezone.datetime(2015, 3, 24, 20), timezone.utc))
        c = Client()
        r = c.post(reverse("vhmstudent_login"), {"username": "admin", "password": "supersecret", "next": "/"})
        self.assertLess(r.status_code, 400)
        self.client = c

    def test_user_creation(self):
        r = self.client.post(reverse("vhmapi_admin_user_all"), {
            "name": "test_user",
            "display_name": "Testy user",
            "max_vhosts": -1,
            "status": "S"
        })
        self.assertLess(r.status_code, 400, r.content)
        user = User.objects.get(name="test_user")
        r = self.client.post(reverse("vhmapi_admin_user_one_password", kwargs={"pk": user.pk}), {
            "password": "abc123",
        })
        self.assertLess(r.status_code, 400, r.content)
        user = User.objects.get(name="test_user")
        self.assertTrue(hashers.check_password("abc123", user.password))

    def test_user_update(self):
        user = User.objects.get(name="test")
        r = self.client.post(reverse("vhmapi_admin_user_one", kwargs={"pk": user.pk}), {
            "display_name": "Yabba my icin",
        })
        self.assertLess(r.status_code, 400, r.content)
        user = User.objects.get(name="test")
        self.assertEqual(user.display_name, "Yabba my icin")

    def test_user_password_change(self):
        user = User.objects.get(name="test")
        r = self.client.post(reverse("vhmapi_admin_user_one_password", kwargs={"pk": user.pk}), {
            "password": "abc456",
        })
        self.assertLess(r.status_code, 400, r.content)
        user = User.objects.get(name="test")
        self.assertTrue(hashers.check_password("abc456", user.password))

    def test_rent_creation(self):
        r = self.client.post(reverse("vhmapi_admin_rent_all"), {
            "user": User.objects.get(name="test").pk,
            "vhost": Vhost.objects.get(name="bumblr").pk,
            "package": SitePackage.objects.get(name="wordpress").pk,
            "start_date": "2015-03-24T21:10:00Z",
            "end_date": "2015-03-24T22:10:00Z",
            "status": "W",
        })
        self.assertLess(r.status_code, 400, r.content)
        rent = VhostRent.objects.get(start_date="2015-03-24T21:10:00Z")
        self.assertEqual(rent.status, VhostRent.WAITING)

    def test_rent_update(self):
        rent = VhostRent.objects.get(status="P")
        r = self.client.post(reverse("vhmapi_admin_rent_one", kwargs={"pk": rent.pk}), {
            "start_date": "2015-03-24T19:00:00Z",
            "status": "W",
        })
        self.assertLess(r.status_code, 400, r.content)
        rent = VhostRent.objects.get(status="W")
        self.assertEqual(rent.status, VhostRent.WAITING)

    def test_vhost_creation(self):
        r = self.client.post(reverse("vhmapi_admin_vhost_all"), {
            "name": "gomblr",
            "interface": None,
            "port": 5678,
            "options": "All",
            "allow_override": "All",
        })
        self.assertLess(r.status_code, 400, r.content)
        vhost = Vhost.objects.get(name="gomblr")
        self.assertEqual(vhost.allow_override, "All")

    def test_vhost_update(self):
        vhost = Vhost.objects.get(name="bumblr")
        r = self.client.post(reverse("vhmapi_admin_vhost_one", kwargs={"pk": vhost.pk}), {
            "options": "None"
        })
        self.assertLess(r.status_code, 400, r.content)
        vhost = Vhost.objects.get(name="bumblr")
        self.assertEqual(vhost.options, "None")

    def test_package_creation(self):
        file = StringIO.StringIO()
        file.name = "dumbfile.zip"
        zip = zipfile.ZipFile(file, "w")
        zip.writestr("dumbfile.txt", "Hello server!")
        zip.close()
        file.pos = 0
        r = self.client.post(reverse("vhmapi_admin_package_all"), {
            "name": "testydoo",
            "description": "A test package",
            "package": file,
        })
        self.assertLess(r.status_code, 400, r.content)
        package = SitePackage.objects.get(name="testydoo")
        self.assertIsNotNone(package.package)
        path = os.path.join(settings.MEDIA_ROOT, package.package.name)
        os.remove(path)