from django.http.response import HttpResponseForbidden
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin, CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, \
    DestroyModelMixin
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from vhmapi.admin.serializers import VhostSerializer, VhostRentSerializer, SitePackageSerializer, UserSerializer, UserPasswordSerializer, \
    UserGroupSerializer, VhostRentGroupSerializer
from vhmapi.shared.views import OptionMixin
from vhmapi.student.views import LoginAPIMixin
from vhmdb.models.rent import VhostRent
from vhmdb.models.sitepackage import SitePackage
from vhmdb.models.user import User, UserGroup
from vhmdb.models.vhost import Vhost


class AdminAPIMixin(APIView):

    def dispatch(self, request, *args, **kwargs):

        if not isinstance(request.vhm_user, User) or not request.vhm_user.is_admin():
            return HttpResponseForbidden()

        return super(AdminAPIMixin, self).dispatch(request, *args, **kwargs)


class UserAllView(GenericAPIView, AdminAPIMixin, ListModelMixin, CreateModelMixin):

    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def pre_save(self, obj):
        obj.set_password(obj.password)


class UserOptionsView(GenericAPIView, AdminAPIMixin, OptionMixin):

    def get_queryset(self):
        return User.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list_option(request, *args, **kwargs)


class UserOneView(GenericAPIView, AdminAPIMixin, RetrieveModelMixin, UpdateModelMixin):

    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return self.update(request, *args, **kwargs)


class UserOnePasswordView(AdminAPIMixin):

    def get_object(self, pk):
        try:
            return User.objects.get(pk=pk)
        except User.DoesNotExist:
            return None

    def post(self, request, *args, **kwargs):
        pk = kwargs.get("pk")
        user = self.get_object(pk)
        pw = request.DATA.get("password", None)
        if not pw:
            return Response({"detail": "Password not given"}, status=HTTP_400_BAD_REQUEST)
        user.set_password(pw)
        user.save()
        ser = UserSerializer(user)
        return Response(ser.data)


class UserGroupAllView(GenericAPIView, AdminAPIMixin, ListModelMixin, CreateModelMixin):

    serializer_class = UserGroupSerializer

    def get_queryset(self):
        return UserGroup.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class UserGroupOptionsView(GenericAPIView, AdminAPIMixin, OptionMixin):

    def get_queryset(self):
        return UserGroup.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list_option(request, *args, **kwargs)


class UserGroupOneView(GenericAPIView, AdminAPIMixin, RetrieveModelMixin, UpdateModelMixin):

    serializer_class = UserGroupSerializer

    def get_queryset(self):
        return UserGroup.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return self.update(request, *args, **kwargs)


class VhostAllView(GenericAPIView, AdminAPIMixin, ListModelMixin, CreateModelMixin):

    serializer_class = VhostSerializer

    def get_queryset(self):
        return Vhost.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class VhostOneView(GenericAPIView, AdminAPIMixin, RetrieveModelMixin, UpdateModelMixin):

    serializer_class = VhostSerializer

    def get_queryset(self):
        return Vhost.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return self.update(request, *args, **kwargs)


class VhostRentAllView(GenericAPIView, AdminAPIMixin, ListModelMixin, CreateModelMixin):

    serializer_class = VhostRentSerializer

    def get_queryset(self):
        return VhostRent.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class VhostRentForGroupView(AdminAPIMixin):

    def post(self, request, *args, **kwargs):
        ""
        # TODO


class VhostRentOneView(GenericAPIView, AdminAPIMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin):

    serializer_class = VhostRentSerializer

    def get_queryset(self):
        return VhostRent.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class VhostRentCancelView(GenericAPIView, AdminAPIMixin):

    serializer_class = VhostRentSerializer

    def get_queryset(self):
        return VhostRent.objects.all()

    def post(self, request, *args, **kwargs):
        obj = self.get_object()
        obj.status = VhostRent.PASSED
        obj.save()
        s = self.get_serializer(obj)
        return Response(s.data)


class SitePackageAllView(GenericAPIView, AdminAPIMixin, ListModelMixin, CreateModelMixin):

    serializer_class = SitePackageSerializer

    def get_queryset(self):
        return SitePackage.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class SitePackageOneView(GenericAPIView, AdminAPIMixin, RetrieveModelMixin, UpdateModelMixin):

    serializer_class = SitePackageSerializer

    def get_queryset(self):
        return SitePackage.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        kwargs["partial"] = True
        return self.update(request, *args, **kwargs)