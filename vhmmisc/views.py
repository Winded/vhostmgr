import json
from django.conf import settings
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect, HttpResponseNotFound, Http404, HttpResponseForbidden
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from django.views.generic.base import View, RedirectView, ContextMixin
from django.views.generic.edit import FormView
from django.utils.translation import ugettext_lazy as _, ugettext
from vhmdb.models.user import User
from vhmdb.models.vhost import Vhost
from vhmmisc import forms
from vhmmisc.auth import login, logout, login_required
from vhmmisc.requests import get_request


class TitleMixin(ContextMixin):

    title = "Untitled"

    def get_title(self):
        return self.title

    def get_context_data(self, **kwargs):
        data = super(TitleMixin, self).get_context_data(**kwargs)
        data["title"] = self.get_title()
        return data


class ListNameMixin(ContextMixin):

    list_name = "object_list"

    def get_context_data(self, **kwargs):
        data = super(ListNameMixin, self).get_context_data(**kwargs)
        data[self.list_name] = data.get("object_list", None)
        return data


class NoCacheMixin(View):

    @method_decorator(never_cache)
    def dispatch(self, request, *args, **kwargs):
        return super(NoCacheMixin, self).dispatch(request, *args, **kwargs)


class LoginMixin(View):

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginMixin, self).dispatch(request, *args, **kwargs)


class AdminMixin(LoginMixin):

    def dispatch(self, request, *args, **kwargs):

        if not isinstance(request.vhm_user, User) or not request.vhm_user.is_admin():
            return HttpResponseForbidden()

        return super(AdminMixin, self).dispatch(request, *args, **kwargs)


class SubmitBtnMixin(ContextMixin):

    submit_btn_text = "Submit"
    submit_btn_type = "default"

    def get_context_data(self, **kwargs):
        data = super(SubmitBtnMixin, self).get_context_data(**kwargs)
        data["submit_btn_text"] = self.submit_btn_text
        data["submit_btn_type"] = self.submit_btn_type
        return data


class MessageMixin(FormView):
    """
    Adds a message on successful form post (when being redirected in POST response).
    """

    message = "You forgot to override the message."
    message_type = messages.INFO

    def form_valid(self, form):

        msg = unicode(self.message)
        messages.add_message(get_request(), message=msg, level=self.message_type)

        return super(MessageMixin, self).form_valid(form)


class SettingsMixin(ContextMixin):

    def get_context_data(self, **kwargs):
        data = super(SettingsMixin, self).get_context_data(**kwargs)
        data["settings"] = settings
        return data


class AngularMixin(ContextMixin):

    ng_app = "angularApp"
    script_url = "js/script.js"

    def get_ng_app(self):
        return self.ng_app

    def get_script_url(self):
        return self.script_url

    def get_ng_constants(self):
        return {}

    def get_context_data(self, **kwargs):
        data = super(AngularMixin, self).get_context_data(**kwargs)
        data["ng_app"] = self.get_ng_app()
        data["ng_constants"] = json.dumps(self.get_ng_constants())
        data["script_url"] = self.get_script_url()
        return data