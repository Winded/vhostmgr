# coding=utf-8
from random import randrange
import string


class ParsingError(Exception):
    pass


def convert_nasty_characters(txt):

    illegal = u"äöå- "
    illegal = [ord(c) for c in illegal]
    legal = u"aoa__"
    deleted = u"."

    trans = dict(zip(illegal, legal))
    for c in deleted:
        trans[ord(c)] = None

    txt = txt.translate(trans)

    return txt


def create_namelist(text):

    if not isinstance(text, unicode):
        raise ParsingError("Namelist needs to be a unicode string.")

    rlist = []

    text = text.strip()

    lines = text.split("\n")

    for line in lines:

        name = line.strip()
        username = convert_nasty_characters(name.lower())

        rlist.append([username, name])

    return rlist


def random_text(length=6, uppercase=True, lowercase=True, digits=True):

    chars = ""

    if uppercase:
        chars += string.uppercase
    if lowercase:
        chars += string.lowercase
    if digits:
        chars += string.digits

    txt = ""

    for i in range(0, length):
        txt += chars[randrange(0, len(chars))]

    return txt