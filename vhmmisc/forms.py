from django import forms
from django.core.exceptions import ValidationError
from django.forms.forms import BaseForm, Form
from django.utils.translation import ugettext_lazy, ugettext as _
from vhmmisc.auth import authenticate
from vhmmisc.requests import get_request


class ClassMixin(BaseForm):

    widget_class = "form-control input-sm"

    def __init__(self, *args, **kwargs):

        super(ClassMixin, self).__init__(*args, **kwargs)

        for n, f in self.fields.iteritems():
            f.widget.attrs["class"] = self.widget_class


class LoginForm(Form, ClassMixin):

    username = forms.CharField(label=ugettext_lazy("Username"))
    password = forms.CharField(label=ugettext_lazy("Password"),
                               widget=forms.PasswordInput)

    next = forms.CharField(widget=forms.HiddenInput, required=False)

    def __init__(self, initial=None, *args, **kwargs):

        next = get_request().GET.get("next", "/")
        initial = initial or {}
        initial["next"] = next

        super(LoginForm, self).__init__(initial=initial, *args, **kwargs)

    def clean(self):

        data = super(LoginForm, self).clean()

        username = data.get("username", None)
        password = data.get("password", None)
        user = authenticate(username=username, password=password)

        if not user:
            raise ValidationError(_("Invalid username or password."))

        return data