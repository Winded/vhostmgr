
import os
import grp
from copy import copy

from limyapi.exceptions import CommandFailedError
from limyapi.savable import SavableObject
from limyapi.transactions import get_transaction


class Group(SavableObject):

    __classname__ = "LinuxGroup"

    def __init__(self, grp=None, gid=None, name=None,
                 password=None, members=None):

        if grp:

            self.gid = grp.gr_gid
            self.name = grp.gr_name
            self.password = grp.gr_passwd

            super(Group, self).__init__(created=True)

        else:

            self.gid = gid
            self.name = name
            self.password = password

            super(Group, self).__init__(created=False)

    def __str__(self):
        if self.name:
            return self.name
        else:
            return super(Group, self).__str__()

    def __eq__(self, other):

        if not isinstance(other, Group):
            return False

        return self.is_created() and \
            other.is_created() and \
            self.gid == other.gid and \
            self.name == other.name

    def _validate(self):
        if not self.name: return False
        return True

    def get_members(self):

        mems = []

        for member in grp.getgrgid(self.gid).gr_mem:
            mem = User.getn(member)
            mems.append(mem)

        return mems

    def _create(self):

        cmd = "groupadd '%(name)s' \
               %(gid)s \
               %(password)s"

        ctx = {
            "gid": "",
            "name": self.name,
            "password": "",
        }

        if self.gid:
            ctx["gid"] = "--gid %i" % self.gid

        if self.password and self.password != "x" and self.password != "*":
            ctx["password"] = "--password '%s'" % self.password

        r = os.system(cmd % ctx)

        if r != 0:
            raise CommandFailedError()

        self.gid = Group.getn(self.name).gid

        get_transaction().mark_change(Group.delete, copy(self))

    def _modify(self):

        cmd = "groupmod '%(old_name)s' \
               --gid %(gid)i \
               --new-name '%(name)s'"

        ctx = {
            "old_name": self.original("name"),
            "gid": self.gid,
            "name": self.name,
            "password": self.password,
        }

        result = os.system(cmd % ctx)

        if self.password != "x" and self.password != "*":
            cmd2 = "groupmod --password '%(password)s' '%(name)s'"
            result2 = os.system(cmd2 % ctx)
        else:
            result2 = 0

        if result != 0 and result2 != 0:
            raise CommandFailedError()

        def undo(self):

            ctx = {
                "old_name": self.name,
                "gid": self.original("gid"),
                "name": self.original("name"),
                "password": self.original("password"),
            }

            os.system(cmd % ctx)

        get_transaction().mark_change(undo, copy(self))

    def delete(self):

        cmd = "groupdel '%(name)s'"

        ctx = {"name": self.original("name")}

        r = os.system(cmd % ctx)

        if r != 0:
            raise CommandFailedError()

        super(Group, self).delete()

        get_transaction().mark_change(Group._create, copy(self))

    @staticmethod
    def get(gid):

        try:
            gr = grp.getgrgid(gid)
        except KeyError:
            return None

        g = Group(grp=gr)

        return g

    @staticmethod
    def getn(name):

        try:
            gr = grp.getgrnam(name)
        except KeyError:
            return None

        g = Group(grp=gr)

        return g

    @staticmethod
    def get_for_user(user):
        """Get all groups where the specified user is in."""

        groups = []

        for gr in grp.getgrall():
            if user in gr.gr_mem:
                groups.append(Group(grp=gr))

        return groups

from limyapi.linux.user import User