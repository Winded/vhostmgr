"""
Contains utilities to manage Linux users
"""
from copy import copy

import os
import pwd
import crypt
import subprocess
from limyapi.exceptions import CommandFailedError
from limyapi.savable import SavableObject
from limyapi.transactions import get_transaction


def change_password(user, new_password):

    devnull = open("/dev/null", "r")
    process = subprocess.Popen(("passwd", user), stdin=subprocess.PIPE, stdout=devnull.fileno(),
                               stderr=subprocess.STDOUT)

    process.communicate((new_password + "\n") * 2)

    if process.returncode != 0:
        return False

    return True


class User(SavableObject):

    __classname__ = "LinuxUser"

    def __init__(self, pwd=None, uid=None, name=None,
                passwd=None, homedir=None, shell="/bin/bash"):

        if pwd:

            self.uid = pwd.pw_uid
            self.name = pwd.pw_name
            self.passwd = pwd.pw_passwd
            self.homedir = pwd.pw_dir
            self.shell = pwd.pw_shell

            super(User, self).__init__(created=True)

        else:

            self.uid = uid
            self.name = name
            self.passwd = passwd
            self.homedir = homedir
            self.shell = shell

            if not self.homedir and self.name:
                self.homedir = "/home/%s" % self.name

            super(User, self).__init__(created=False)

    def __str__(self):
        if self.name:
            return self.name
        else:
            return super(User, self).__str__()

    def __eq__(self, other):

        if not isinstance(other, User): return False

        return self.is_created() and \
            other.is_created() and \
            self.uid == other.uid and \
            self.name == other.name

    def _validate(self):
        if not self.name: return False
        if not self.homedir: return False
        if not self.shell: return False
        return True

    def get_groups(self):
        return Group.get_for_user(self.name)

    def set_groups(self, groups):

        if not self.is_created():
            raise Exception("User must be created before setting groups.")

        old_groups = self.get_groups()

        gs = []
        for g in groups:
            gs.append(str(g.gid))

        cmd = "usermod %(name)s " \
              "--groups '%(groups)s'"

        ctx = {
            "name": self.original("name"),
            "groups": ",".join(gs),
        }

        r = os.system(cmd % ctx)

        if r != 0:
            raise CommandFailedError()

        get_transaction().mark_change(User.set_groups, copy(self), old_groups)

    def add_to_group(self, group):
        groups = self.get_groups()
        groups.append(group)
        self.set_groups(groups)

    def remove_from_group(self, group):

        groups = self.get_groups()

        if group in groups:
            groups.remove(group)
            self.set_groups(groups)

    def _create(self):

        cmd = """useradd \
        %(uid)s \
        --home-dir "%(homedir)s" \
        --create-home \
        --shell "%(shell)s" \
        "%(name)s" """

        ctx = {
            "uid": "",
            "name": self.name,
            "homedir": self.homedir,
            "shell": self.shell,
        }

        if self.uid != None:
            ctx["uid"] = "--uid %i" % self.uid

        result = os.system(cmd % ctx)

        if result != 0:
            raise CommandFailedError()

        if self.passwd and self.passwd != "x" and self.passwd != "*":
            change_password(self.name, self.passwd)

        self.uid = User.getn(self.name).uid

        get_transaction().mark_change(User.delete, copy(self))

    def _modify(self):

        cmd = """usermod \
        --uid %(uid)i \
        --login "%(name)s" \
        --home "%(homedir)s" \
        --shell "%(shell)s" \
        "%(old_name)s" """

        ctx = {
            "uid": self.uid,
            "name": self.name,
            "old_name": self.original("name"),
            "password": self.passwd,
            "homedir": self.homedir,
            "shell": self.shell,
        }

        result = os.system(cmd % ctx)

        if self.passwd != "x" and self.passwd != "*":
            change_password(self.name, self.passwd)

        if result != 0:
            raise CommandFailedError()

        def undo(self):

            ctx = {
                "uid": self.original("uid"),
                "name": self.original("name"),
                "old_name": self.name,
                "password": self.original("passwd"),
                "homedir": self.original("homedir"),
                "shell": self.original("shell"),
            }

            os.system(cmd % ctx)

            change_password(self.original("name"), self.original("passwd"))

        get_transaction().mark_change(undo, copy(self))

    def delete(self):
        """Delete the user."""

        # Probably useless but just in case
        if self.original("name") == "root":
            return

        cmd = "userdel --remove '%(name)s'"

        ctx = {"name": self.original("name")}

        result = os.system(cmd % ctx)

        if result != 0:
            raise CommandFailedError()

        super(User, self).delete()

        # For the sake of convenience, we remove the user's own group aswell
        g = Group.getn(self.original("name"))
        if g:
            g.delete()

        get_transaction().mark_change(User._create, copy(self))

    @staticmethod
    def get(uid):
        """Get a linux user by it's UID"""

        try:
            pwu = pwd.getpwuid(uid)
        except KeyError:
            return None

        u = User(pwd=pwu)

        return u

    @staticmethod
    def getn(name):
        """Get a linux user by it's name"""

        try:
            pwu = pwd.getpwnam(name)
        except KeyError:
            return None

        u = User(pwd=pwu)

        return u

    @staticmethod
    def get_all():

        all = []

        for pw in pwd.getpwall():
            all.append(User(pwd=pw))

        return all

    @staticmethod
    def exists(uid):
        """Check if user of the given UID exists on the system"""

        try:
            pwu = pwd.getpwuid(uid)
            return True
        except KeyError:
            return False

    @staticmethod
    def existsn(name):
        """Check if user of the given name exists on the system"""

        try:
            pwu = pwd.getpwnam(name)
            return True
        except KeyError:
            return False

from limyapi.linux.group import Group