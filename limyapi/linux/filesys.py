"""
Contains utilities for creating files, directories and symlinks
"""

import os

class Folder(object):
    """
    Represents a folder. This can also be a symlink,
    but for better symlink control, use the Symlink class.
    """

    def __init__(self, path):

        self.path = os.path.abspath(path)

    def exists(self):
        return os.path.exists(self.path) and os.path.isdir(self.path)

    def save(self):
        """Create the directory if it doesn't exist."""

        if self.exists(): return

        if os.path.exists(self.path): # exists() passed, so if the path exists, it's not a directory
            raise IOError("Path is a file, not a directory")

        os.path.mkdir(self.path)

    def delete(self):
        """Delete everything inside the folder and the folder itself."""

        if not self.exists(): return

        self.clear()

        os.path.rmdir(self.path)

    def clear(self):
        """Delete everything inside the folder, but not the folder itself."""

        if not self.exists(): return

        for root, dirs, files in os.walk(self.path, topdown = False):
            for f in files:
                os.remove(os.path.join(self.path, root, f))
            for d in dirs:
                os.rmdir(os.path.join(self.path, root, d))


class Symlink(Folder):
    """
    Represents a symbolic link
    """

    def __init__(self, path, source):

        self.source = source

        super(Symlink, self).__init__(path)

    def exists(self):
        e = super(Symlink, self).exists()
        return e and os.path.islink(self.path)

    def save(self):

        if self.exists(): return

        if os.path.exists(self.path): # exists() passed, so if the path exists, it's not a directory
            raise IOError("Path is a file or directory, not a symlink")

        os.symlink(self.path, self.source)

    def delete(self):

        if not self.exists(): return

        os.remove(self.path)


def sys(cmd):

    r = os.system(cmd)

    if r != 0:
        return False

    return True

def copy(source, dest):
    """Copy a file"""
    return sys("cp %s %s" % (source, dest))

def delete(source):
    """Delete a file"""
    return sys("rm %s" % source)

def erase_dir(path):
    """
    Erases a directory entirely. Wrapper for rm.
    We have root permissions, so take care..
    """

    return sys("rm -rf %s" % path)

def clear_dir(path):
    """
    Erase the contents of a directory without removing
    the directory itself.
    """

    return sys("rm -rf %s/*" % path)

def copy_dir(source, dest):
    """
    Copies the given directory to the destination. Wrapper for cp.
    """

    return sys("cp -r %s %s" % (source, dest))

def pack_dir(source, dest):
    """
    Pack a directory to a tar file.
    """
    # TODO
    pass

def chown_dir(username, path):
    return sys("chown -R %s %s" % (username, path))

def chmod_dir(permissions, path):
    return sys("chmod -R %s %s" % (permissions, path))

def create_link(real_path, link_path):
    """
    Create a symlink.
    """

    try:
        os.symlink(real_path, link_path)
    except IOError:
        return False

    return True

def delete_link(link_path):
    """
    Delete a symlink. 
    """

    if not os.path.isdir(link_path): # Symlinks are identified as directories
        return False

    return sys("rm %s" % link_path)