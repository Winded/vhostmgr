from unittest import TestCase
from unittest.loader import TestLoader
import sys
from unittest.runner import TextTestRunner
from limyapi import LinuxUser, LinuxGroup, MySQLUser, MySQLDatabase, MySQLDatabasePriv
from limyapi.mysql.user import make_password
from limyapi.transactions import get_transaction


class UserTests(TestCase):

    def setUp(self):
        pass

    def tearDown(self):

        user = LinuxUser.getn("testy_tester")
        if user is not None:
            user.delete()

        user = MySQLUser.get("testy_tester")
        if user is not None:
            user.delete()

        get_transaction().commit()

    def test_linux(self):

        # Test creation
        user = LinuxUser(name="testy_tester")
        user.passwd = "Password123"
        user.save()

        user = LinuxUser.getn("testy_tester")
        self.assertIsNotNone(user, "Failed to create Linux user!")

        # Test modification
        user.shell = "/bin/sh"
        user.passwd = "Passwordier"
        user.save()

        user = LinuxUser.getn("testy_tester")
        self.assertIsNotNone(user, "Failed to modify Linux user!")
        self.assertEqual(user.shell, "/bin/sh", "Failed to modify Linux user!")

        # Test deletion
        user.delete()
        user = LinuxUser.getn("testy_tester")
        self.assertIsNone(user, "Failed to delete Linux user!")

        # Test rollback
        get_transaction().rollback()
        user = LinuxUser.getn("testy_tester")
        self.assertIsNone(user, "Failed to rollback Linux user!")

    def test_mysql(self):

        # Test creation
        user = MySQLUser()
        user.name = "testy_tester"
        user.password = make_password("Password123")
        user.save()

        user = MySQLUser.get("testy_tester")
        self.assertIsNotNone(user, "Failed to create MySQL user!")

        # Test modification
        user.name = "testy_testier"
        user.password = make_password("Passwordier")
        user.save()

        user = MySQLUser.get("testy_testier")
        self.assertIsNotNone(user, "Failed to modify MySQL user!")

        # Test deletion
        user.delete()
        user = MySQLUser.get("testy_testier")
        self.assertIsNone(user, "Failed to delete MySQL user!")

        # Test rollback
        get_transaction().rollback()
        user = MySQLUser.get("testy_tester")
        self.assertIsNone(user, "Failed to rollback MySQL user!")
        user = MySQLUser.get("testy_testier")
        self.assertIsNone(user, "Failed to rollback MySQL user!")


class GroupTests(TestCase):

    def setUp(self):

        user = LinuxUser(name="testy_tester")
        user.passwd = "Password123"
        user.save()
        self.user = user

        get_transaction().commit()

    def tearDown(self):

        user = LinuxUser.getn("testy_tester")
        if user is not None:
            user.delete()
        get_transaction().commit()

    def test_groups(self):

        # Test creation
        group = LinuxGroup(name="testy_group")
        group.save()

        group = LinuxGroup.getn("testy_group")
        self.assertIsNotNone(group, "Failed to create Linux group!")

        user = LinuxUser.getn("testy_tester")

        # Test member add
        user.add_to_group(group)

        usergroups = user.get_groups()
        self.assertIn(group, usergroups, "Failed to add user to group!")

        # Test member remove
        user.remove_from_group(group)

        usergroups = user.get_groups()
        self.assertNotIn(group, usergroups, "Failed to remove user from group!")

        # Test group modify
        group.name = "testier_group"
        group.save()

        group = LinuxGroup.getn("testier_group")
        self.assertIsNotNone(group, "Failed to modify Linux group!")

        # Test group delete
        group.delete()

        group = LinuxGroup.getn("testier_group")
        self.assertIsNone(group, "Failed to delete Linux group!")

        # Test rollback
        get_transaction().rollback()
        group = LinuxGroup.getn("testier_group")
        self.assertIsNone(group, "Failed to rollback Linux group!")


class DatabaseTests(TestCase):

    def setUp(self):

        user = MySQLUser()
        user.name = "testy_tester"
        user.password = make_password("Password123")
        user.save()

        get_transaction().commit()

    def tearDown(self):

        user = MySQLUser.get("testy_tester")
        if user is not None:
            user.delete()
        get_transaction().commit()

    def test_database(self):

        # Test create
        db = MySQLDatabase(name="testy_db")
        db.save()

        db = MySQLDatabase.get("testy_db")
        self.assertIsNotNone(db, "Failed to create MySQL database!")

        # Test add privilege
        priv = MySQLDatabasePriv(db="testy_db", user="testy_tester")
        priv.save()

        priv = MySQLDatabasePriv.get("testy_tester", "testy_db")
        self.assertIsNotNone(priv, "Failed to create MySQL privilege!")

        # Test remove privilege
        priv.delete()

        priv = MySQLDatabasePriv.get("testy_tester", "testy_db")
        self.assertIsNone(priv, "Failed to remove MySQL privilege!")

        # Test delete
        db.delete()

        db = MySQLDatabase.get("testy_db")
        self.assertIsNone(db, "Failed to delete MySQL database!")

        # Test rollback
        get_transaction().rollback()


def run_tests():
    """
    Runs tests to see if limyapi is working as expected.
    WARNING: Not designed to be used in production. Run during setup, before having any important data.
    """
    suite = TestLoader().loadTestsFromModule(sys.modules[__name__])
    runner = TextTestRunner(verbosity=2)
    runner.run(suite)