
class SavableObject(object):
    """
    An object designed as a proxy object; save() saves the object,
    get() gets an existing object, is_created() says if the object
    is created or not.
    """

    __classname__ = "Object"

    def __init__(self, created=False):
        self._created = created

    def __repr__(self):
        return "<%s: %s>" % (self.__classname__, self.__str__())

    def __setattr__(self, key, value):

        or_key = "_or_%s" % key

        if not key.startswith("_") and value is not None and not hasattr(self, or_key):
            setattr(self, or_key, value)

        super(SavableObject, self).__setattr__(key, value)

    def original(self, key):
        """
        Retrieve the original value of an attribute,
        or none if it is not set.
        """

        or_key = "_or_%s" % key

        return getattr(self, or_key, None)

    def _validate(self):
        """Is the data valid for creating or modifying?"""
        return True

    def is_created(self):
        """Is the object created on remote source?"""
        return self._created

    def _create(self):
        raise NotImplementedError()

    def _modify(self):
        raise NotImplementedError()

    def save(self):
        """
        Save the object to the remote source.
        """

        if not self._validate():
            raise Exception("Not all properties are set.")

        if not self._created:

            self._create()
            self._created = True

        else:

            self._modify()

        for att in dir(self):

            if att.startswith("_"): continue

            if self.original(att) != None:
                setattr(self, "_or_%s" % att, getattr(self, att))

    def delete(self):
        """
        Delete the object from the remote source.
        """

        self._created = False