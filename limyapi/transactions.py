from copy import copy
from types import FunctionType


_transaction = None


def get_transaction():
    global _transaction
    if not _transaction:
        _transaction = Transaction()
    return _transaction


class Transaction(object):
    """
    Represents a transaction for proxy-style objects, with logging capabilities.
    Proxy objects register changes to transaction object with a rollback function,
    and the transaction object will call the rollback functions if something goes wrong later.
    """

    def __init__(self):
        self._rolling_back = False
        self._rollback_methods = []

    def _reset(self):
        self._rollback_methods = []

    def mark_change(self, rollback_method, *args, **kwargs):
        """
        Puts the given rollback function to the rollback list.
        """
        if self._rolling_back:
            return
        if not hasattr(rollback_method, "__call__"):
            raise TypeError("%s is not a function." % rollback_method)
        self._rollback_methods.append((rollback_method, args, kwargs))

    def rollback(self):
        """
        Rolls back marked changes from last commit or transaction creation.
        This basically calls the rollback methods in a reverse order.
        """
        self._rolling_back = True
        rbmethods = copy(self._rollback_methods)
        rbmethods.reverse()
        for rb in rbmethods:
            method = rb[0]
            args = rb[1]
            kwargs = rb[2]
            method.__call__(*args, **kwargs)
        self._rolling_back = False
        self._reset()

    def commit(self):
        self._reset()