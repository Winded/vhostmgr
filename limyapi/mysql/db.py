from copy import copy
from limyapi.savable import SavableObject
from limyapi.mysql import cur
from limyapi.transactions import get_transaction


class Database(SavableObject):

    __classname__ = "MySQLDatabase"

    def __init__(self, name = None, **kwargs):

        self.name = name

        super(Database, self).__init__(**kwargs)

    def __str__(self):
        if self.name:
            return self.name
        else:
            return super(Database, self).__str__()

    def __eq__(self, other):

        if not isinstance(other, Database): return False

        return self.is_created() and \
            other.is_created() and \
            self.name == other.name

    def _validate(self):
        if not self.name: return False
        return True

    def get_privs(self):
        return DatabasePriv.get_for_db(self.name)

    def _create(self):

        cmd = "CREATE DATABASE %(name)s;"

        ctx = {"name": self.name}

        cur().execute(cmd % ctx)

        get_transaction().mark_change(Database.delete, copy(self), delete_privs=False)

    def _modify(self):

        if self.name != self.original("name"):
            raise Exception("Databases cannot be renamed.")

    def delete(self, delete_privs=True):

        if delete_privs:
            for priv in self.get_privs():
                priv.delete()

        cmd = "DROP DATABASE %(name)s;"

        ctx = {"name": self.name}

        cur().execute(cmd % ctx)

        get_transaction().mark_change(Database._create, copy(self))

        super(Database, self).delete()

    def recreate(self):
        """Deletes the database, and creates it again, removing ALL data."""

        self.delete(delete_privs=False)
        self.save()

    @staticmethod
    def get(name):

        cmd = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA \
               WHERE SCHEMA_NAME = %(name)s;"

        ctx = {"name": name}

        c = cur()

        c.execute(cmd, ctx)
        r = c.fetchone()

        if not r: return None

        db = Database(created=True)
        db.name = r[0]

        return db


class DatabasePriv(SavableObject):

    def __init__(self, db=None, user=None,  **kwargs):

        self.db = db
        self.user = user

        super(DatabasePriv, self).__init__(**kwargs)

    def _validate(self):
        if not self.db: return False
        if not self.user: return False
        return True

    def _create_new(self, hosts):

        cmd = "INSERT INTO db (Host, User, Db, \
               Select_priv, Insert_priv, Update_priv, \
               Delete_priv, Create_priv, Drop_priv,  \
               Grant_priv, References_priv, Index_priv, \
               Alter_priv, Create_tmp_table_priv, \
               Lock_tables_priv, Create_view_priv, \
               Show_view_priv, Create_routine_priv, \
               Alter_routine_priv, \
               Execute_priv, Event_priv, \
               Trigger_priv) \
               VALUES (%(host)s, %(user)s, %(db)s, \
               'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', \
               'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y');"

        for host in hosts:

            ctx = {
                "host": host,
                "user": self.user,
                "db": self.db,
            }

            cur().execute(cmd, ctx)

    def _delete_old(self):

        cmd = "DELETE FROM db \
               WHERE User=%(user)s AND Db=%(db)s;"

        ctx = {
            "user": self.original("user"),
            "db": self.original("db"),
        }

        cur().execute(cmd, ctx)

    def _create(self):

        user = User.get(self.user)
        if not user:
            raise Exception("User %s does not exist." % self.user)
        db = Database.get(self.db)
        if not db:
            raise Exception("Database %s does not exist." % self.db)

        self._create_new(user.hosts)

        get_transaction().mark_change(DatabasePriv.delete, copy(self))

        cur().execute("FLUSH PRIVILEGES;")

    def _modify(self):

        user = User.get(self.user)
        if not user:
            raise Exception("User %s does not exist." % self.user)
        db = Database.get(self.db)
        if not db:
            raise Exception("Database %s does not exist." % self.db)

        if self.original("user") != self.user or self.original("db") != self.db:

            self._delete_old()
            self._create_new(user.hosts)

            def revert(self, hosts):
                user = self.user
                db = self.db
                self.user = self.original("user")
                self.db = self.original("db")
                self._or_user = user
                self._or_db = db
                self._delete_old()
                self._create_new(hosts)
                cur().execute("FLUSH PRIVILEGES;")

            get_transaction().mark_change(revert, self, user.hosts)

        cur().execute("FLUSH PRIVILEGES;")

    def delete(self):

        cmd = "DELETE FROM db \
               WHERE User=%(user)s AND Db=%(db)s"

        ctx = {
            "user": self.user,
            "db": self.db,
        }

        cur().execute(cmd, ctx)

        cur().execute("FLUSH PRIVILEGES;")

        get_transaction().mark_change(DatabasePriv._create, copy(self))

        super(DatabasePriv, self).delete()

    def get_db(self):
        return Database.get(self.db)

    def get_user(self):
        return User.get(self.user)

    @staticmethod
    def get(user, db):

        cmd = "SELECT User, Db FROM db \
               WHERE Db=%(db)s AND User=%(user)s \
               GROUP BY User;"

        ctx = {
            "db": db,
            "user": user,
        }

        c = cur()

        c.execute(cmd, ctx)

        r = c.fetchone()
        if not r:
            return None

        priv = DatabasePriv(created = True)
        priv.db = r[1]
        priv.user = r[0]

        return priv

    @staticmethod
    def get_for_db(name):

        cmd = "SELECT User, Db FROM db \
               WHERE Db=%(name)s GROUP BY User;"

        ctx = { "name": name }

        c = cur()

        c.execute(cmd, ctx)
        r = c.fetchall()

        privs = []

        for ro in r:

            p = DatabasePriv(created = True)
            p.db = name
            p.user = ro[0]

            privs.append(p)

        return privs

    @staticmethod
    def get_for_user(name):

        cmd = "SELECT User, Db FROM db \
               WHERE User=%(name)s GROUP BY Db;"

        ctx = { "name": name }

        c = cur()

        c.execute(cmd, ctx)

        r = c.fetchall()

        privs = []

        for ro in r:

            p = DatabasePriv(created = True)
            p.db = ro[1]
            p.user = name

            privs.append(p)

        return privs

from limyapi.mysql.user import User
