
import MySQLdb

import atexit

from warnings import filterwarnings
from django.conf import settings

filterwarnings("ignore", category=MySQLdb.Warning)

connect_info = {
    "host": "localhost",
    "port": 3306,
    "user": "nobody",
    "passwd": "nada",
}

_cur = None
_db = None


def conn():

    if not _db:
        connect()

    return _db


def cur():

    if not _cur:
        connect()

    return _cur


def connect():
    global _db, _cur

    _db = MySQLdb.connect(db="mysql", **connect_info)
    _db.autocommit(False)
    _cur = _db.cursor()


def disconnect():
    if _cur:
        _cur.close()
    if _db:
        _db.close()

atexit.register(disconnect)
