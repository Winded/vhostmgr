"""
Contains utilities to manage MySQL users
"""
from copy import copy

import hashlib

from limyapi.savable import SavableObject
from limyapi.mysql import cur
from limyapi.transactions import get_transaction


def make_password(raw_password):

    p1 = hashlib.sha1(raw_password).digest()
    p2 = hashlib.sha1(p1).hexdigest()
    p = "*" + p2.upper()

    return p


class User(SavableObject):

    __classname__ = "MySQLUser"

    def __init__(self, hosts=None,
                 name=None, password=None, **kwargs):

        if not hosts:
            hosts = ["localhost", "%"]

        self.hosts = hosts
        self.name = name
        self.password = password

        super(User, self).__init__(**kwargs)

    def __str__(self):
        if self.name:
            return self.name
        else:
            return super(User, self).__str__()

    def __eq__(self, other):

        if not isinstance(other, User): return False

        return self.is_created() and \
            other.is_created() and \
            self.name == other.name

    def _validate(self):
        if not self.hosts: return False
        if not self.name: return False
        if not self.password: return False
        return True

    def get_privs(self):
        return DatabasePriv.get_for_user(self.original("name"))

    def set_password(self, raw_password):
        self.password = make_password(raw_password)

    def _create_host(self, host):

        cmd = "INSERT INTO \
            user (Host, User, Password, ssl_cipher, x509_issuer, x509_subject) \
            VALUES (%(host)s, %(name)s, %(password)s, '', '', '');"

        ctx = {
            "host": host,
            "name": self.name,
            "password": self.password,
        }

        cur().execute(cmd, ctx)

        cur().execute("FLUSH PRIVILEGES;")

        get_transaction().mark_change(User._delete_host, copy(self), host)

    def _delete_host(self, host):

        cmd = "DELETE FROM user \
            WHERE Host=%(host)s AND User=%(name)s;"

        ctx = {
            "host": host,
            "name": self.original("name"),
        }

        cur().execute(cmd, ctx)

        cur().execute("FLUSH PRIVILEGES;")

        get_transaction().mark_change(User._create_host, copy(self), host)

    def _create(self):

        for host in self.hosts:
            self._create_host(host)

        cur().execute("FLUSH PRIVILEGES;")

    def _modify(self):

        old = User.get(self.original("name"))

        # Remove old hosts
        for host in old.hosts:
            if not host in self.hosts:
                self._delete_host(host)

        # Create new hosts
        for host in self.hosts:
            if not host in old.hosts:
                self._create_host(host)

        cmd = "UPDATE user \
                SET User=%(name)s, Password=%(password)s \
                WHERE User=%(old_name)s"

        ctx = {
            "old_name": old.name,
            "name": self.name,
            "password": self.password,
        }

        cur().execute(cmd, ctx)

        def revert(self, old):
            ctx = {
                "old_name": self.name,
                "name": old.name,
                "password": old.password,
            }
            cur().execute(cmd, ctx)
            cur().execute("FLUSH PRIVILEGES;")

        get_transaction().mark_change(revert, copy(self), copy(old))

        cur().execute("FLUSH PRIVILEGES;")

    def delete(self, delete_privs = True):

        if not self.is_created():
            raise Exception("Cannot delete non-created user.")

        if delete_privs:
            for priv in self.get_privs():
                priv.delete()

        old = User.get(self.original("name"))

        for host in old.hosts:
            self._delete_host(host)

        super(User, self).delete()

    @staticmethod
    def get(name):
        """Get a MySQL user by name"""

        cmd = "SELECT Host, User, Password FROM user \
            WHERE User=%(name)s;"

        ctx = {
            "name": name,
        }

        c = cur()
        c.execute(cmd, ctx)
        r = c.fetchall()

        if len(r) == 0:
            return None

        fr = r[0]

        u = User(name = fr[1], password = fr[2], created = True)

        u.hosts = []
        for ro in r:
            u.hosts.append(ro[0])

        return u

    @staticmethod
    def exists(name):
        return User.get(name) != None

from limyapi.mysql.db import Database, DatabasePriv