"""
Contains methods to manage
    - Linux users and groups
    - Linux files and folder
    - MySQL users and databases
All in an OO manner.
"""

from limyapi import linux, mysql
from django.conf import settings

mysql.connect_info = {
    "host": settings.DATABASES["mysql"]["HOST"],
    "port": settings.DATABASES["mysql"]["PORT"],
    "user": settings.DATABASES["mysql"]["USER"],
    "passwd": settings.DATABASES["mysql"]["PASSWORD"],
}

from limyapi.linux.user import User as LinuxUser
from limyapi.linux.group import Group as LinuxGroup

from limyapi.mysql.user import User as MySQLUser
from limyapi.mysql.db import Database as MySQLDatabase
from limyapi.mysql.db import DatabasePriv as MySQLDatabasePriv