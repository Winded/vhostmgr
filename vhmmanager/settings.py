import MySQLdb
from vhmweb.settings import *

INSTALLED_APPS = ["vhmdb"]

LOGGING["handlers"]["file"]["filename"] = "/etc/vhostmgr.log"