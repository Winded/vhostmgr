import logging


class IgnoreType(logging.Filter):
    def __init__(self, type, *args, **kwargs):
        super(IgnoreType, self).__init__(*args, **kwargs)
        self.type = type

    def filter(self, record):
        if not record.exc_info:
            return True
        type = record.exc_info[0]
        return type != self.type