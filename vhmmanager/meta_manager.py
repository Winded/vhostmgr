"""
Meta Manager enables and disables rents when it's time to activate/deactivate them.
"""
from datetime import datetime
from django.utils import timezone
from vhmmanager import logger
from vhmmanager.db.models import VhostRent
from django.db.transaction import commit_on_success


@commit_on_success
def manage_rents():

    d = timezone.now()

    new_rents = VhostRent.objects.filter(status=VhostRent.WAITING,
                                         start_date__lt=d, end_date__gt=d)
    old_rents = VhostRent.objects.filter(status=VhostRent.ACTIVE,
                                         end_date__lt=d)

    oc, nc = new_rents.count(), old_rents.count()
    if oc + nc > 0:
        logger.info("%i new rents, %i old rents" % (oc, nc))

    for rent in new_rents:
        rent.enable()
    for rent in old_rents:
        rent.disable()