import codecs
from django.template import Template, Context


def file_to_file(source, dest, ctx=None):
    """
    Generate destination file from source template file with the given context.
    """

    if not ctx:
        ctx = {}

    f = file(source, "r")

    temp = Template(f.read())
    ctx = Context(ctx)

    result = temp.render(ctx)

    rf = codecs.open(dest, "wb", "utf-8")
    rf.write(result)
    rf.flush()