
def has_changed(instance, field):

    if not instance.pk:
        return False

    old_value = getattr(instance.__class__._default_manager.get(pk=instance.pk), field)
    new_value = getattr(instance, field)

    if hasattr(new_value, "file"):
        # Handle FileFields as special cases, because the uploaded filename could be
        # the same as the filename that's already there even though there may
        # be different file contents.
        from django.core.files.uploadedfile import UploadedFile
        return isinstance(new_value.file, UploadedFile)

    return not new_value == old_value