"""
Miscellaneous utilities.
"""
from datetime import datetime


def name_to_username(txt):
    """
    Convert spaces to underscores, remove dashes,
    lowercase and replace scandinavian letters,
    to make the text username-friendly.
    """

    r = [
        [" ", "_"],
        ["-", ""],
        ["\xc3\xa5", "a"],
        ["\xc3\xa4", "a"],
        ["\xc3\xb6", "o"],
    ]

    txt = txt.strip().lower()

    for rep in r:
        txt = txt.replace(*rep)

    return txt


def parse_name_list(list):

    r = []

    slist = list.split("\n")
    for name in slist:

        if not name.strip(): continue

        n = name_to_username(name)
        if not n: continue

        r.append((n, name))

    return r


def mkdate(text):
    return datetime.strptime(text, "%d.%m.%Y").date()


def mktime(text):
    return datetime.strptime(text, "%H:%M").time()