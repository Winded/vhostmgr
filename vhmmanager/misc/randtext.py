from random import randrange
import string


def make(length=6, uppercase=True, lowercase=True, digits=True):
    
    chars = ""

    if uppercase:
        chars += string.uppercase
    if lowercase:
        chars += string.lowercase
    if digits:
        chars += string.digits

    txt = ""

    for i in range(length + 1):
        txt += chars[randrange(0, len(chars))]

    return txt