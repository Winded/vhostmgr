"""
Contains utilities to manage network interface configuration files.
E.g. /etc/network/interfaces
"""

import os

from django.conf import settings
from vhmmanager.misc import template


def generate(interfaces, dns_settings):
    """
    Generate interfaces configuration from the given data.
    Interfaces should be a loopable list of Interface database entries.
    """

    ctx = {
        "interfaces": interfaces,
        "dns": dns_settings
    }

    template.file_to_file(settings.VHOST_IFCONFIG_TEMPLATE, settings.VHOST_IFCONFIG_PATH, ctx)