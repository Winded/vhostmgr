"""
Contains utilities to manage Apache VirtualHost configuration files
"""

import os
from limyapi.linux import filesys

from django.conf import settings
from vhmmanager.misc import template


def generate_vhost_config(vhosts):

    ctx = {
        "vhosts": vhosts,
        "vhost_folder": settings.VHOST_WWW_PATH,
    }

    path = os.path.join(settings.VHOST_APACHE_CONF_PATH, "sites-available", "vhosts")

    template.file_to_file(settings.VHOST_APACHE_TEMPLATE, path, ctx)


def generate_vhost_info(vhost, rent, user, dest):

    ctx = {
        "vhost": vhost,
        "rent": rent,
        "user": user,
    }

    template.file_to_file(settings.VHOST_INFO_TEMPLATE, dest, ctx)


def enable_site(sitename):

    spath = os.path.join(settings.VHOST_APACHE_CONF_PATH, "sites-available", sitename)
    dpath = os.path.join(settings.VHOST_APACHE_CONF_PATH, "sites-enabled", sitename)

    filesys.copy(spath, dpath)


def disable_site(sitename):

    path = os.path.join(settings.VHOST_APACHE_CONF_PATH, "sites-enabled", sitename)

    filesys.delete(path)