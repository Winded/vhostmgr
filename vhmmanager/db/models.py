"""
Serverside extensions for models in vhmdb
"""
import os
from zipfile import ZipFile
from django.conf import settings
from django.db.transaction import commit_on_success
import limyapi
from limyapi import MySQLDatabase, LinuxUser, LinuxGroup, MySQLDatabasePriv, MySQLUser
from vhmmanager import template
from vhmdb import models


from vhmdb.models.user import User, UserGroup
from vhmdb.models.vhost import Vhost, Interface
from vhmdb.models.sitepackage import SitePackage
from vhmdb.models.rent import VhostRent


def make_vhost_configs():
    """
    Generate new apache site configuration.
    """

    vhosts = Vhost.objects.all()

    template.apache.generate_vhost_config(vhosts)


def make_interface_configs():
    """
    Generate new interface configurations.
    """

    ifs = Interface.objects.all()

    template.ifconfig.generate(ifs, settings.VHOST_IFCONFIG_DNS_SERVERS)


def deploy_package(sp, path):
    """Deploy a site package to the given path"""

    zf = ZipFile(sp.package)
    zf.extractall(path)


def chown_path(path, user, group):
    """
    chown the given path to the given user and group
    """
    limyapi.linux.filesys.chown_dir("%s:%s" % (user, group), path)


class UserMeta(models.UserMeta):

    class Meta:
        proxy = True

    def _update_linux(self):

        if not self.user:

            if self.linux_user:
                u = LinuxUser.getn(self.linux_user)
                u.delete()
                self.linux_user = None

            return

        uname = self.user.name

        # If linux_user field is empty, it means that it has not been created yet.
        if not self.linux_user:

            u = LinuxUser.getn(uname)
            if u:
                raise Exception("LinuxUser %s already exists" % uname)

            u = LinuxUser(name=uname)
            u.save()

            self.linux_user = uname
            self.save()

        # Change Linux user name if needed
        elif uname != self.linux_user:

            u = LinuxUser.getn(self.linux_user)
            u.name = uname
            u.save()

            self.linux_user = uname
            self.save()

        # Change Linux user password if needed
        if self.linux_password:
            u = LinuxUser.getn(uname)
            u.passwd = self.linux_password
            u.save()
            self.linux_password = None
            self.save()

    def _update_mysql(self):

        if not self.user:

            if self.mysql_user:
                u = MySQLUser.get(self.mysql_user)
                u.delete()
                self.mysql_user = None

            return

        uname = self.user.name

        if not self.mysql_user:

            u = MySQLUser.get(uname)
            if u:
                raise Exception("MySQLUser %s already exists." % uname)

            u = MySQLUser(name=uname, password=self.mysql_password)
            u.save()

            self.mysql_user = uname
            self.save()

        elif uname != self.mysql_user:

            u = MySQLUser.get(uname)
            u.name = uname
            u.save()

            self.mysql_user = uname
            self.save()

        if self.mysql_password:
            u = MySQLUser.get(uname)
            u.password = self.mysql_password
            u.save()
            self.mysql_password = None
            self.save()

    @commit_on_success
    def update_backend(self):
        """
        Update the status of non-database content accordingly to the database.
        For users, this is linux user, mysql user and their passwords.
        """

        self._update_linux()
        self._update_mysql()

        if not self.user:
            self.delete()


class VhostMeta(models.VhostMeta):

    class Meta:
        proxy = True

    def _update_database(self):

        if not self.vhost:
            if self.database:
                db = MySQLDatabase.get(self.database)
                db.delete()
                self.database = None
            return

        vname = self.vhost.name

        if not self.database:

            db = MySQLDatabase.get(vname)
            if db:
                raise Exception("MySQL database %s already exists" % vname)

            db = MySQLDatabase(name=vname)
            db.save()

            self.database = vname
            self.save()

        elif self.database != vname:

            # Databases can't be renamed; so we do nothing
            pass

    def _update_group(self):

        if not self.vhost:
            if self.group:
                g = LinuxGroup.getn(self.group)
                g.delete()
                self.group = None
            return

        gname = "vhostmgr_%s" % self.vhost.name

        if not self.group:

            g = LinuxGroup.getn(gname)
            if g:
                raise Exception("Linux group %s already exists" % gname)

            g = LinuxGroup(name=gname)
            g.save()

            wd = LinuxUser.getn("www-data")
            wd.add_to_group(g)

            self.group = gname
            self.save()

        elif self.group != gname:

            g = LinuxGroup.getn(self.group)
            g.name = gname
            g.save()

            self.group = gname
            self.save()

    def _update_www_path(self):

        if not self.vhost:
            if self.www_path:
                limyapi.linux.filesys.erase_dir(self.www_path)
                self.www_path = None
            return

        if not self.www_path:

            path = self.vhost.get_www_path()

            if not os.path.exists(path):
                os.mkdir(path)

            self.www_path = path

            if self.group:
                chown_path(path, "www-data", self.group)
                limyapi.linux.filesys.chmod_dir(770, path)

            self.save()

    @commit_on_success
    def update_backend(self):
        """
        Update the status of non-database content accordingly to the database.
        For vhosts, this is databases, Linux groups and WWW folder.
        """

        self._update_database()
        self._update_group()
        self._update_www_path()

        if not self.vhost:
            self.delete()


class VhostRentMeta(models.VhostRentMeta):

    class Meta:
        proxy = True

    def _update_www_path(self):

        if not self.rent:

            if self.www_path:

                limyapi.linux.filesys.clear_dir(self.www_path)
                self.www_path = None

            return

        active = self.rent.status == VhostRent.ACTIVE

        if active and not self.www_path:

            real_path = self.rent.vhost.get_www_path()

            vipath = os.path.join(real_path, "vhost_info.html")
            template.apache.generate_vhost_info(self.rent.vhost, self.rent, self.rent.user, vipath)

            if self.rent.package:
                deploy_package(self.rent.package, real_path)

            if self.linux_group:
                chown_path(real_path, "www-data", self.linux_group)

            self.www_path = real_path
            self.save()

        elif not active and self.www_path:

            limyapi.linux.filesys.clear_dir(self.www_path)

            self.www_path = None
            self.save()

    def _update_symlink(self):

        if not self.rent:

            if self.symlink:

                limyapi.linux.filesys.delete_link(self.symlink)
                self.symlink = None

            return

        active = self.rent.status == VhostRent.ACTIVE

        if active and not self.symlink:

            real_path = self.rent.vhost.get_www_path()
            link_path = self.rent.get_www_symlink()

            limyapi.linux.filesys.create_link(real_path, link_path)

            self.symlink = link_path
            self.save()

        elif not active and self.symlink:

            real_path = self.rent.vhost.get_www_path()

            limyapi.linux.filesys.delete_link(self.symlink)

            self.symlink = None
            self.save()

    def _update_linux(self):

        if not self.rent:

            if self.linux_group and self.linux_user:

                g = LinuxGroup.getn(self.linux_group)

                u = LinuxUser.getn(self.linux_user)
                u.remove_from_group(g)
                self.linux_group = None
                self.linux_user = None

            return

        active = self.rent.status == VhostRent.ACTIVE

        gname = "vhostmgr_%s" % self.rent.vhost.name
        uname = self.rent.user.name

        if active and not self.linux_group and not self.linux_user:

            g = LinuxGroup.getn(gname)

            u = LinuxUser.getn(uname)
            u.add_to_group(g)

            self.linux_group = gname
            self.linux_user = uname
            self.save()

        elif not active and self.linux_group and self.linux_user:

            g = LinuxGroup.getn(gname)

            u = LinuxUser.getn(uname)
            u.remove_from_group(g)

            self.linux_group = None
            self.linux_user = None
            self.save()

    def _update_mysql(self):

        if not self.rent:

            if self.mysql_db and self.mysql_user:

                priv = MySQLDatabasePriv.get(self.mysql_user, self.mysql_db)
                priv.delete()
                self.mysql_db = None
                self.mysql_user = None

            return

        active = self.rent.status == VhostRent.ACTIVE

        uname = self.rent.user.name
        dbname = self.rent.vhost.name

        if active and not self.mysql_db and not self.mysql_user:

            priv = MySQLDatabasePriv()
            priv.user = uname
            priv.db = dbname
            priv.save()

            self.mysql_user = uname
            self.mysql_db = dbname
            self.save()

        elif not active and self.mysql_db and self.mysql_user:

            priv = MySQLDatabasePriv.get(uname, dbname)
            if priv:
                priv.delete()
            db = MySQLDatabase.get(dbname)
            db.recreate()

            self.mysql_user = None
            self.mysql_db = None
            self.save()

    @commit_on_success
    def update_backend(self):
        """
        Update the status of non-database content accordingly to the database.
        For vhost rents, this is the database privilege and linux user-group relationship.
        """

        self._update_linux()
        self._update_mysql()
        self._update_www_path()
        self._update_symlink()

        if not self.rent:
            self.delete()