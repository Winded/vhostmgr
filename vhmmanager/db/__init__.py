"""
Contains django database configuration
"""
import logging
from limyapi.mysql import conn
from limyapi.transactions import get_transaction
from vhmmanager import logger
from django.core import management
from vhmmanager.db import models
from vhmmanager.db.models import VhostRentMeta, UserMeta, VhostMeta


def sync():

    management.call_command("syncdb")


def update_backend(item=None):

    try:

        if item and item.meta:
            item.meta.update_backend()
            return
        elif item:
            return

        items = []

        def add(qs):
            for obj in qs:
                items.append(obj)

        add(VhostRentMeta.objects.filter(rent=None))
        add(VhostMeta.objects.filter(vhost=None))
        add(UserMeta.objects.filter(user=None))

        add(UserMeta.objects.exclude(user=None))
        add(VhostMeta.objects.exclude(vhost=None))
        add(VhostRentMeta.objects.exclude(rent=None))

        for item in items:
            try:
                item.update_backend()
                get_transaction().commit()
            except:
                logger.exception("Backend update failed")
                get_transaction().rollback()

    except:
        logger.exception("Backend update failed")