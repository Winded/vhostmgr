VhostManager
====================
VhostManager is a management application that turns a linux Apache-MySQL-PHP setup into a renting based web hosting system. It is designed for educational purposes to easily set up and manage virtual servers for practicing anything related to web hosting - web development, Linux SSH usage, MySQL usage and SQL scripting, and so on.

Features:

 - Manage VirtualHosts, their databases and folders, Linux users and MySQL users through a single interface
 - Manage usage of VirtualHosts by users with a renting system; VirtualHosts do not have to be created singularily for each user, making deployment of a web site easy as pie
 - Two interfaces; one is a command line client (when everything fails), and an easy-to-use web interface.

The server program requires root permissions to operate, and is not guaranteed not to cause explosions,
so backup your system before using this software.