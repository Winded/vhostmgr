from django.conf.urls import patterns, include, url
from vhmadmin import views

urlpatterns = patterns('',

    url(r"^$", views.Index.as_view(), name="vhmadmin_index"),

    url(r"^rents/$", views.RentList.as_view(), name="vhmadmin_rent_list"),
    url(r"^rents/new$", views.RentCreate.as_view(), name="vhmadmin_rent_create"),
    url(r"^rents/group_new$", views.RentGroupCreate.as_view(), name="vhmadmin_rent_group_create"),
    url(r"^rents/(?P<slug>\d+)/edit$", views.RentEdit.as_view(), name="vhmadmin_rent_edit"),
    url(r"^rents/(?P<slug>\d+)/delete$", views.RentDelete.as_view(), name="vhmadmin_rent_delete"),
    url(r"^rents/(?P<slug>\d+)/cancel$", views.RentCancel.as_view(), name="vhmadmin_rent_cancel"),
    url(r"^rents/(?P<slug>\d+)/accept$", views.RentAccept.as_view(), name="vhmadmin_rent_accept"),
    url(r"^rents/(?P<slug>\d+)/decline$", views.RentReject.as_view(), name="vhmadmin_rent_decline"),

    url(r"^users/$", views.UserList.as_view(), name="vhmadmin_user_list"),
    url(r"^users/(?P<slug>\d+)/$", views.UserDetail.as_view(), name="vhmadmin_user_detail"),
    url(r"^users/new$", views.UserCreate.as_view(), name="vhmadmin_user_create"),
    url(r"^users/group_new$", views.UserGroupCreate.as_view(), name="vhmadmin_user_group_create"),
    url(r"^users/(?P<slug>\d+)/edit$", views.UserEdit.as_view(), name="vhmadmin_user_edit"),
    url(r"^users/(?P<slug>\d+)/edit/password$", views.UserEditPassword.as_view(), name="vhmadmin_user_edit_password"),
    url(r"^users/(?P<slug>\d+)/delete$", views.UserDelete.as_view(), name="vhmadmin_user_delete"),

    url(r"^groups/$", views.GroupList.as_view(), name="vhmadmin_group_list"),
    url(r"^groups/(?P<slug>\d+)/$", views.GroupDetail.as_view(), name="vhmadmin_group_detail"),
    url(r"^groups/new", views.GroupCreate.as_view(), name="vhmadmin_group_create"),
    url(r"^groups/(?P<slug>\d+)/pwgen$", views.GroupPasswordGenerate.as_view(), name="vhmadmin_group_pwgen"),
    url(r"^groups/pwgen_result$", views.GroupPasswordGenerateResult.as_view(), name="vhmadmin_group_pwgen_result"),
    url(r"^groups/(?P<slug>\d+)/edit$", views.GroupEdit.as_view(), name="vhmadmin_group_edit"),
    url(r"^groups/(?P<slug>\d+)/delete$", views.GroupDelete.as_view(), name="vhmadmin_group_delete"),

    url(r"^vhosts/$", views.VhostList.as_view(), name="vhmadmin_vhost_list"),
    url(r"^vhosts/(?P<slug>\d+)/$", views.VhostDetail.as_view(), name="vhmadmin_vhost_detail"),
    url(r"^vhosts/new", views.VhostCreate.as_view(), name="vhmadmin_vhost_create"),
    url(r"^vhosts/group_new", views.VhostGroupCreate.as_view(), name="vhmadmin_vhost_group_create"),
    url(r"^vhosts/(?P<slug>\d+)/edit$", views.VhostEdit.as_view(), name="vhmadmin_vhost_edit"),
    url(r"^vhosts/(?P<slug>\d+)/delete$", views.VhostDelete.as_view(), name="vhmadmin_vhost_delete"),

    url(r"^packages/$", views.SitePackageList.as_view(), name="vhmadmin_package_list"),
    url(r"^packages/new", views.SitePackageCreate.as_view(), name="vhmadmin_package_create"),
    url(r"^packages/(?P<slug>\d+)/edit$", views.SitePackageEdit.as_view(), name="vhmadmin_package_edit"),
    url(r"^packages/(?P<slug>\d+)/update$", views.SitePackageUpdate.as_view(), name="vhmadmin_package_update"),
    url(r"^packages/(?P<slug>\d+)/delete$", views.SitePackageDelete.as_view(), name="vhmadmin_package_delete"),

)