from django.http.response import HttpResponseForbidden
from django.utils.translation import ugettext_lazy as _
from django.views.generic.base import TemplateView
from vhmdb.models.user import User
from vhmstudent.views import TitleMixin, LoginMixin