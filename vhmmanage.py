#!/usr/bin/env python

from argparse import ArgumentParser
from getpass import getpass
import logging
import os
import sys

if __name__ == "__main__":

    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "vhmmanager.settings")

    from vhmmanager import db, meta_manager
    from vhmmanager.db.models import User, Vhost, Interface, make_vhost_configs, make_interface_configs, VhostMeta
    from limyapi import tests

    logger = logging.getLogger("vhostmgr")

    argp = ArgumentParser()

    sp = argp.add_subparsers(dest="action", help="The action to perform.")
    sp.add_parser("update_vhosts", help="Generate configuration files.")
    sp.add_parser("setup_db", help="Setup the database.")
    sp.add_parser("test_backend", help="Run backend tests. WARNING: Not designed for use in production. "
                                       "Use only durin setup or in development.")
    sp.add_parser("meta_update", help="Perform metadata update and backend update. "
                                      "Don't run this manually if you have a crontab running for it. "
                                      "Simultaneous runs can cause problems.")

    argp.add_argument("-l", "--log", action="store_true", default=False,
                      help="Write messages to log file. Useful when running as cron job")
    argp.add_argument("-d", "--debug", action="store_true", default=False,
                      help="Write debug log messages.")

    r = argp.parse_args()

    action = r.action

    if action == "update_vhosts":

        logger.info("Creating databases and groups...")

        for m in VhostMeta.objects.all():
            m.update_backend()

        logger.info("Creating configs...")

        if Interface.objects.all().count() > 0:
            make_interface_configs()
        else:
            logger.info("No interfaces created; skipping")

        if Vhost.objects.all().count() > 0:
            make_vhost_configs()
        else:
            logger.info("No VirtualHosts created; skipping")

        logger.info("Done.")

    elif action == "setup_db":

        logger.info("-\nSynchronizing database\n-\n")

        db.sync()

        uc = User.objects.filter(admin=True).count()

        if uc == 0:

            logger.info("-\nSetup an administrative user\n-\n")

            username = raw_input("Username: ")
            display_name = raw_input("Display name: ")

            while True:
                passwd = getpass("Password: ")
                rpasswd = getpass("Retype password: ")
                if passwd == rpasswd: break
                logger.info("Passwords do not match.")

            user = User()
            user.name = username
            user.display_name = display_name
            user.set_password(passwd)
            user.admin = True

            user.save()

            user.meta.update_backend()

        logger.info("\n--\nDatabase setup complete")

    elif action == "test_backend":
        tests.run_tests()

    elif action == "meta_update":
        logger.info("Performing meta update..")
        meta_manager.manage_rents()
        logger.info("Performing backend update..")
        db.update_backend()