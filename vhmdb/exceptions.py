"""
This module contains exceptions specific for vhm
"""

from django.db import IntegrityError

class UsernameTakenError(IntegrityError):
	"""
	This error is thrown when the given username has been taken or is unavailable.
	"""
	def __init__(self, name, cannot_use = False):
		if cannot_use:
			msg = "Username %s cannot be used." % name
		else:
			msg = "Username %s is already taken." % name
		self.args = [msg]

class VhostRentLimitError(IntegrityError):
	"""
	This error is thrown when the user's VirtualHost rent limit is
	exceeded.
	"""
	def __init__(self):
		msg = "Maximum vhost rents exceeded."
		self.args = [msg]

class VhostRentDateError(IntegrityError):
	"""
	This error is thrown when vhost renting datetimes overlap.
	"""
	def __init__(self):
		msg = "Cannot overlap renting dates for one vhost."
		self.args = [msg]