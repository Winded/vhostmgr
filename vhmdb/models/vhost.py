import os
from django.conf import settings
from django.db import models, IntegrityError
from django.db.models import Q
from django.db.models.signals import pre_delete, post_save
from django.dispatch.dispatcher import receiver
from django.utils.translation import ugettext as _
from django.core.exceptions import ValidationError
from vhmdb.models import create_meta


class Interface(models.Model):
    """
    Represents a virtual network interface.
    """

    class Meta:
        app_label = "vhmdb"
        db_table = "vhm_interface"

    device = models.CharField(max_length=32)

    ip_address = models.CharField(max_length=32, unique=True)
    netmask = models.CharField(max_length=32)
    gateway = models.CharField(max_length=32)

    def __unicode__(self):
        return "%s (%s)" % (self.get_name(), self.ip_address)

    def get_name(self):
        if not self.id: return self.device
        return "%s:%i" % (self.device, self.id)

    @staticmethod
    def get_by_name(name):

        sname = name.split(":")
        if len(sname) < 2:
            return None

        try:
            id = int(sname[1])
        except ValueError:
            return None

        iface = Interface.objects.filter(device=sname[0], id=id)
        if not iface.exists():
            return None

        return iface[0]


class Vhost(models.Model):
    """
    Represents a VirtualHost.
    """

    class Meta:
        app_label = "vhmdb"
        db_table = "vhm_vhost"

    name = models.CharField(max_length=255, unique=True)

    interface = models.ForeignKey(Interface, null=True, blank=True)
    port = models.PositiveIntegerField(default=80)

    options = models.TextField(help_text="Options for Apache vhost config.",
                               default="All")
    allow_override = models.TextField(help_text="AllowOverride for Apache vhost config.",
                                      default="All")

    def __unicode__(self):

        iface = "*"
        if self.interface:
            iface = self.interface.ip_address

        return "%s (%s:%s)" % (self.name, iface, self.port)

    def is_rented_at(self, start_date, end_date, exclude=None):
        """
        Check if the VirtualHost is rented during the given time.
        Rents given in exclude parameter are not taken into account.
        """

        exclude = exclude or []
        exclude = [r.pk for r in exclude]

        rents = VhostRent.objects.filter(vhost=self, status__in=VhostRent.STATUS_CURRENT)

        for r in rents:

            if r.pk in exclude:
                continue

            if r.start_date <= start_date <= r.end_date \
                    or r.start_date <= end_date <= r.end_date \
                    or start_date <= r.start_date <= end_date \
                    or start_date <= r.end_date <= end_date:
                return True

        return False

    def is_active(self):
        """
        Check if the VirtualHost is currently rented.
        """
        return VhostRent.objects.filter(status=VhostRent.ACTIVE, vhost=self).exists()

    def get_www_path(self):
        """Get the path to the vhost's www folder"""
        return os.path.join(settings.VHOST_WWW_PATH, ".real", "%i" % self.id)

    def get_address_port(self):

        addr = "*"
        if self.interface:
            addr = self.interface.ip_address

        return "%s:%s" % (addr, self.port)

    def get_is_active_display(self):
        return _("Yes") if self.is_active() else _("No")

    def set_options(self, options):
        """Set options from an array."""

        opt = " ".join(options)
        self.options = opt

    def set_allow_override(self, allow_override):
        """Set allow_override from an array."""

        ao = " ".join(allow_override)
        self.allow_override = ao

    def clean(self):

        if self.port and self.port > 65535:
            raise ValidationError(_("Port must be less than or equal to 65535."))

    @staticmethod
    def get_available_at(start_date, end_date):
        """
        Get a VirtualHost that is available for rent for the given time period.
        """

        vhosts = Vhost.objects.all()

        for vh in vhosts:
            if not vh.is_rented_at(start_date, end_date):
                return vh

        return None


@receiver(post_save, sender=Vhost)
def vhost_post_save(sender, instance, **kwargs):
    create_meta(instance, VhostMeta, "vhost")


@receiver(pre_delete, sender=Vhost)
def vhost_pre_delete(sender, instance, **kwargs):

    rents = VhostRent.objects.filter(vhost=instance)
    for r in rents:
        r.delete()

    if instance.interface:
        ivhosts = Vhost.objects.filter(interface=instance.interface).exists()
        if not ivhosts:
            instance.interface.delete()
            instance.interface = None


class VhostMeta(models.Model):

    class Meta:
        app_label = "vhmdb"
        db_table = "vhm_vhost_meta"

    vhost = models.OneToOneField(Vhost, related_name="meta", null=True, on_delete=models.SET_NULL)

    database = models.CharField(max_length=64, null=True)
    group = models.CharField(max_length=255, null=True)
    www_path = models.CharField(max_length=255, null=True)


from vhmdb.models.rent import VhostRent