from django.conf import settings
from django.db import models
from django.contrib.auth import hashers
from django.db.models.signals import post_save, pre_delete
from django.dispatch.dispatcher import receiver
from django.utils import timezone
from django.utils.formats import date_format
from django.utils.translation import ugettext_lazy, ugettext
from limyapi.mysql.user import make_password
from vhmdb.models import username_validator, create_meta
from vhmdb.models.rent import VhostRent


class User(models.Model):
    """
    Represents a user. Both LinuxUser and MySQLUser should be tied to this user.
    """

    class Meta:
        app_label = "vhmdb"
        db_table = "vhm_user"

    STUDENT = "S"
    TEACHER = "T"
    DISABLED = "D"
    ADMIN = "A"
    PRIVILEGED = (ADMIN, TEACHER)
    STATUS_CHOICES = (
        (STUDENT, ugettext_lazy("Student")),
        (TEACHER, ugettext_lazy("Teacher")),
        (DISABLED, ugettext_lazy("Disabled")),
        (ADMIN, ugettext_lazy("Administrator")),
    )

    name = models.CharField(max_length=16, unique=True,
                            validators=[username_validator])
    password = models.CharField(max_length=77)

    display_name = models.CharField(max_length=255)

    max_vhosts = models.IntegerField(default=-1,
                                     help_text="The maximum amount of vhosts the user can rent at once. "
                                               "-1 for default.")

    status = models.CharField(max_length=1, default=STUDENT, choices=STATUS_CHOICES)

    date_created = models.DateTimeField(auto_now_add=True)

    def __init__(self, *args, **kwargs):
        super(User, self).__init__(*args, **kwargs)
        self.raw_password = None

    def __unicode__(self):
        return self.name

    def get_meta(self):
        return UserMeta.objects.get(user=self)

    def get_max_vhosts_display(self):
        if self.max_vhosts > -1:
            return self.max_vhosts
        else:
            return "%i (%s)" % (settings.VHOST_MAX_USER_RENTS, ugettext("Default"))

    def get_date_created_display(self):
        return date_format(timezone.localtime(self.date_created), "DATETIME_FORMAT")

    def is_admin(self):
        return self.status == User.ADMIN

    def is_disabled(self):
        return self.status == User.DISABLED

    def get_privileged_groups(self):

        if self.status == self.TEACHER:
            return self.teacher_groups.all()
        elif self.status == self.ADMIN:
            return UserGroup.objects.all()

        return UserGroup.objects.none()

    def get_privileged_users(self):
        q = User.objects.none()

        if self.status == self.TEACHER:
            for group in self.teacher_groups.all():
                q = q | group.students.exclude(status__in=User.PRIVILEGED)
        elif self.status == self.ADMIN:
            q = User.objects.exclude(status=User.ADMIN)

        return q

    def get_max_vhosts(self):
        if self.max_vhosts >= 0:
            return self.max_vhosts
        else:
            return settings.VHOST_MAX_USER_RENTS

    def can_rent(self):
        """
        Returns true or false if the user can rent any more vhosts.
        Exclude should be an array of vhost objects to ignore on query.
        """

        if self.is_admin():
            return True
        if self.is_disabled():
            return False

        max_vhosts = self.get_max_vhosts()

        rcount = VhostRent.objects.filter(user=self).exclude(
            status__in=VhostRent.STATUS_ENDED).count()

        return rcount < max_vhosts

    def check_password(self, raw_password):
        return hashers.check_password(raw_password, self.password)

    def set_password(self, raw_password):
        """Set the password for the user."""

        self.password = hashers.make_password(raw_password)
        self.raw_password = raw_password


@receiver(post_save, sender=User)
def user_post_save(sender, instance, **kwargs):

    create_meta(instance, UserMeta, "user")

    if instance.raw_password:
        m = instance.meta
        m.set_password(instance.raw_password)
        m.save()


@receiver(pre_delete, sender=User)
def user_pre_delete(sender, instance, **kwargs):
    rents = VhostRent.objects.filter(user=instance)
    for r in rents:
        r.delete()


class UserMeta(models.Model):
    """
    User meta objects are used to track and modify changes made to Linux and MySQL users.
    """

    class Meta:
        app_label = "vhmdb"
        db_table = "vhm_user_meta"

    user = models.OneToOneField(User, related_name="meta", null=True, on_delete=models.SET_NULL)

    linux_user = models.CharField(max_length=255, null=True)
    linux_password = models.CharField(max_length=13, null=True)

    mysql_user = models.CharField(max_length=16, null=True)
    mysql_password = models.CharField(max_length=41, null=True)

    def set_password(self, raw_password):
        self.linux_password = raw_password
        self.mysql_password = make_password(raw_password)


class UserGroup(models.Model):
    """
    User groups are meant for organizing users into groups with a relation,
    like a school class. User groups are not related to linux or mysql.
    """

    class Meta:
        app_label = "vhmdb"
        db_table = "vhm_user_group"

    name = models.CharField(max_length=255, unique=True,
                            validators=[username_validator])

    teachers = models.ManyToManyField(User, related_name="teacher_groups")

    students = models.ManyToManyField(User, related_name="student_groups")

    def __unicode__(self):
        return self.name