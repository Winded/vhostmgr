"""
Defines the Vhost database model.
"""


import string
from django.core.exceptions import ValidationError


def username_validator(value):

    legal = string.lowercase + string.digits + "_"

    for c in value:
        if not c in legal:
            raise ValidationError("Illegal username character %s" % c)


def create_meta(instance, metaclass, ref_key):

    kw = {ref_key: instance}

    created = False
    try:
        m = metaclass.objects.get(**kw)
        created = True
    except metaclass.DoesNotExist:
        pass

    if not created:
        m = metaclass(**kw)
        m.save()


from vhmdb.models.user import User, UserMeta, UserGroup
from vhmdb.models.vhost import Vhost, VhostMeta, Interface
from vhmdb.models.sitepackage import SitePackage
from vhmdb.models.rent import VhostRent, VhostRentMeta