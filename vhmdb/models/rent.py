import os
from datetime import datetime, timedelta
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models, IntegrityError
from django.db.models.signals import post_save
from django.db.transaction import commit_on_success
from django.dispatch.dispatcher import receiver
from django.utils import timezone
from django.utils.formats import date_format
from django.utils.translation import ugettext as _, ugettext_lazy
from vhmdb.models import create_meta
from vhmdb.models.sitepackage import SitePackage
from vhmdb.exceptions import VhostRentLimitError, VhostRentDateError


class VhostRent(models.Model):

    class Meta:
        app_label = "vhmdb"
        db_table = "vhm_vhost_rent"

    ACTIVE = "A"
    PASSED = "E"
    WAITING = "W"
    PENDING = "P"
    REJECTED = "R"
    STATUS_CURRENT = (PENDING, WAITING, ACTIVE)
    STATUS_ENDED = (PASSED, REJECTED)
    STATUS_ANY = (ACTIVE, PASSED, WAITING, PENDING, REJECTED)
    STATUS_CHOICES = (
        (ACTIVE, ugettext_lazy("Active")),
        (PASSED, ugettext_lazy("Passed")),
        (WAITING, ugettext_lazy("Waiting")),
        (PENDING, ugettext_lazy("Pending")),
        (REJECTED, ugettext_lazy("Rejected")),
    )

    vhost = models.ForeignKey("Vhost", related_name="rents")
    user = models.ForeignKey("User", related_name="rents")

    start_date = models.DateTimeField()
    end_date = models.DateTimeField()

    status = models.CharField(max_length=1, choices=STATUS_CHOICES, default=PENDING)

    package = models.ForeignKey(SitePackage, null=True, blank=True, on_delete=models.SET_NULL,
                                help_text="The package that will be deployed when rent starts. "
                                          "NULL for no package.")

    def __unicode__(self):
        return "%s rented by %s" % (self.vhost.name, self.user.name)

    def get_start_date_display(self):
        return date_format(timezone.localtime(self.start_date), "DATETIME_FORMAT")

    def get_end_date_display(self):
        return date_format(timezone.localtime(self.end_date), "DATETIME_FORMAT")

    def should_be_active(self):
        """
        Compares start_date and end_date with the current date, and returns if the rent
        should be active according to the dates or not
        """

        dt = datetime.now()

        if self.start_date <= dt <= self.end_date:
            return True
        else:
            return False

    def get_www_symlink(self):
        """Get the path for the symlink created for this rent"""
        return os.path.join(settings.VHOST_WWW_PATH, "%s_%s" % (self.user.name, self.vhost.name))

    @commit_on_success
    def enable(self):
        """
        Activate the rent. This is triggered at start_date
        """

        self.status = VhostRent.ACTIVE
        self.save()

    @commit_on_success
    def disable(self):
        """
        Deactivate the rent. This is triggered at end_date
        """

        self.status = VhostRent.PASSED
        self.save()

    def clean(self):

        if self.start_date and self.end_date:

            if not self.pk:

                if not self.pk and self.start_date < timezone.now():
                    raise ValidationError(_("Start date/time must be later than current date/time."))

                if self.start_date >= self.end_date:
                    raise ValidationError(_("Start date/time must be less than end date/time."))

            max_rent_time = timedelta(days=settings.VHOST_MAX_RENT_TIME)
            if self.end_date - self.start_date > max_rent_time:
                raise ValidationError(_("Rent duration must not overpass maximum rent duration (%s days)")
                                      % max_rent_time.days)

        exclude = None
        if self.pk is not None:
            exclude = [self]

        if self.vhost and self.vhost.is_rented_at(self.start_date, self.end_date, exclude=exclude):
            raise ValidationError(_("The virtual host is already rented during the given time."))

        if self.user and self.pk is None and not self.user.can_rent():
            raise ValidationError(_("User has reached the maximum amount of current rents."))


@receiver(post_save, sender=VhostRent)
def rent_post_save(sender, instance, **kwargs):
    create_meta(instance, VhostRentMeta, "rent")


class VhostRentMeta(models.Model):

    class Meta:
        app_label = "vhmdb"
        db_table = "vhm_vhost_rent_meta"

    rent = models.OneToOneField(VhostRent, related_name="meta", null=True, on_delete=models.SET_NULL)

    mysql_db = models.CharField(max_length=64, null=True)
    mysql_user = models.CharField(max_length=16, null=True)

    linux_group = models.CharField(max_length=255, null=True)
    linux_user = models.CharField(max_length=255, null=True)

    symlink = models.CharField(max_length=255, null=True)
    www_path = models.CharField(max_length=255, null=True)