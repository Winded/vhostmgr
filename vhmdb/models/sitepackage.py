from zipfile import is_zipfile
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.translation import ugettext
from vhmmisc.utils import random_text
from vhmdb.models import username_validator


def package_upload_to(package, fn):
    return "%s%s" % (random_text(digits=False), package.get_type_display())


class SitePackage(models.Model):

    class Meta:
        app_label = "vhmdb"
        db_table = "vhm_package"

    ZIP = "Z"
    RAR = "R"
    TYPE_CHOICES = (
        (ZIP, ".zip"),
        (RAR, ".rar"),
    )

    name = models.CharField(max_length=255, unique=True,
                            validators=[username_validator])
    description = models.TextField(blank=True)

    type = models.CharField(max_length=1, choices=TYPE_CHOICES, default=ZIP)
    package = models.FileField(upload_to=package_upload_to)

    def __unicode__(self):
        return self.name

    def clean(self):

        if self.type and self.type == SitePackage.RAR:
            raise ValidationError(ugettext("RAR type not yet supported."))

        if self.package and not is_zipfile(self.package):
            raise ValidationError(ugettext("package must be a ZIP file"))