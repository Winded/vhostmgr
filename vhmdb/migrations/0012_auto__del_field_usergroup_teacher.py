# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'UserGroup.teacher'
        db.delete_column('vhm_user_group', 'teacher_id')


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'UserGroup.teacher'
        raise RuntimeError("Cannot reverse this migration. 'UserGroup.teacher' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'UserGroup.teacher'
        db.add_column('vhm_user_group', 'teacher',
                      self.gf('django.db.models.fields.related.ForeignKey')(related_name='teacher_groups', to=orm['vhmdb.User']),
                      keep_default=False)


    models = {
        'vhmdb.interface': {
            'Meta': {'object_name': 'Interface', 'db_table': "'vhm_interface'"},
            'device': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'gateway': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip_address': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'netmask': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'vhmdb.sitepackage': {
            'Meta': {'object_name': 'SitePackage', 'db_table': "'vhm_package'"},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'package': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'Z'", 'max_length': '1'})
        },
        'vhmdb.user': {
            'Meta': {'object_name': 'User', 'db_table': "'vhm_user'"},
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_vhosts': ('django.db.models.fields.IntegerField', [], {'default': '-1'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '77'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'S'", 'max_length': '1'})
        },
        'vhmdb.usergroup': {
            'Meta': {'object_name': 'UserGroup', 'db_table': "'vhm_user_group'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'students': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'student_groups'", 'symmetrical': 'False', 'to': "orm['vhmdb.User']"}),
            'teachers': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'teacher_groups'", 'symmetrical': 'False', 'to': "orm['vhmdb.User']"})
        },
        'vhmdb.usermeta': {
            'Meta': {'object_name': 'UserMeta', 'db_table': "'vhm_user_meta'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'linux_password': ('django.db.models.fields.CharField', [], {'max_length': '13'}),
            'linux_user': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'mysql_password': ('django.db.models.fields.CharField', [], {'max_length': '41'}),
            'mysql_user': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'meta'", 'unique': 'True', 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['vhmdb.User']"})
        },
        'vhmdb.vhost': {
            'Meta': {'object_name': 'Vhost', 'db_table': "'vhm_vhost'"},
            'allow_override': ('django.db.models.fields.TextField', [], {'default': "'All'"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interface': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vhmdb.Interface']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'options': ('django.db.models.fields.TextField', [], {'default': "'All'"}),
            'port': ('django.db.models.fields.PositiveIntegerField', [], {'default': '80'})
        },
        'vhmdb.vhostmeta': {
            'Meta': {'object_name': 'VhostMeta', 'db_table': "'vhm_vhost_meta'"},
            'database': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'group': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vhost': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'meta'", 'unique': 'True', 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['vhmdb.Vhost']"}),
            'www_path': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        'vhmdb.vhostrent': {
            'Meta': {'object_name': 'VhostRent', 'db_table': "'vhm_vhost_rent'"},
            'end_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vhmdb.SitePackage']", 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'P'", 'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vhmdb.User']"}),
            'vhost': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vhmdb.Vhost']"})
        },
        'vhmdb.vhostrentmeta': {
            'Meta': {'object_name': 'VhostRentMeta', 'db_table': "'vhm_vhost_rent_meta'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'linux_group': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'linux_user': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'mysql_db': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'mysql_user': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True'}),
            'rent': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'meta'", 'unique': 'True', 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['vhmdb.VhostRent']"}),
            'symlink': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'www_path': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        }
    }

    complete_apps = ['vhmdb']