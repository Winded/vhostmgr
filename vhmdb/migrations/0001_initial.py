# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'SitePackage'
        db.create_table('vhm_package', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(default='Z', max_length=1)),
            ('package', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal('vhmdb', ['SitePackage'])

        # Adding model 'VhostRent'
        db.create_table('vhm_vhost_rent', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vhost', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vhmdb.Vhost'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vhmdb.User'])),
            ('start_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('end_date', self.gf('django.db.models.fields.DateTimeField')()),
            ('status', self.gf('django.db.models.fields.CharField')(default='P', max_length=1)),
            ('package', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vhmdb.SitePackage'], null=True, blank=True)),
        ))
        db.send_create_signal('vhmdb', ['VhostRent'])

        # Adding model 'VhostRentMeta'
        db.create_table('vhm_vhost_rent_meta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('rent', self.gf('django.db.models.fields.related.OneToOneField')(related_name='meta', unique=True, null=True, on_delete=models.SET_NULL, to=orm['vhmdb.VhostRent'])),
            ('mysql_db', self.gf('django.db.models.fields.CharField')(max_length=64, null=True)),
            ('mysql_user', self.gf('django.db.models.fields.CharField')(max_length=16, null=True)),
            ('linux_group', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('linux_user', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('symlink', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('www_path', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal('vhmdb', ['VhostRentMeta'])

        # Adding model 'User'
        db.create_table('vhm_user', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=16)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=77)),
            ('display_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('max_vhosts', self.gf('django.db.models.fields.IntegerField')(default=-1)),
            ('admin', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('disabled', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('date_created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal('vhmdb', ['User'])

        # Adding model 'UserMeta'
        db.create_table('vhm_user_meta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(related_name='meta', unique=True, null=True, on_delete=models.SET_NULL, to=orm['vhmdb.User'])),
            ('linux_user', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('linux_password', self.gf('django.db.models.fields.CharField')(max_length=13)),
            ('mysql_user', self.gf('django.db.models.fields.CharField')(max_length=16, null=True)),
            ('mysql_password', self.gf('django.db.models.fields.CharField')(max_length=41)),
        ))
        db.send_create_signal('vhmdb', ['UserMeta'])

        # Adding model 'UserGroup'
        db.create_table('vhm_user_group', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
        ))
        db.send_create_signal('vhmdb', ['UserGroup'])

        # Adding M2M table for field users on 'UserGroup'
        m2m_table_name = db.shorten_name('vhm_user_group_users')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('usergroup', models.ForeignKey(orm['vhmdb.usergroup'], null=False)),
            ('user', models.ForeignKey(orm['vhmdb.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['usergroup_id', 'user_id'])

        # Adding model 'Interface'
        db.create_table('vhm_interface', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('device', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('ip_address', self.gf('django.db.models.fields.CharField')(unique=True, max_length=32)),
            ('netmask', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('gateway', self.gf('django.db.models.fields.CharField')(max_length=32)),
        ))
        db.send_create_signal('vhmdb', ['Interface'])

        # Adding model 'Vhost'
        db.create_table('vhm_vhost', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('interface', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['vhmdb.Interface'], null=True, blank=True)),
            ('port', self.gf('django.db.models.fields.PositiveIntegerField')(default=80)),
            ('options', self.gf('django.db.models.fields.TextField')(default='All')),
            ('allow_override', self.gf('django.db.models.fields.TextField')(default='All')),
        ))
        db.send_create_signal('vhmdb', ['Vhost'])

        # Adding model 'VhostMeta'
        db.create_table('vhm_vhost_meta', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vhost', self.gf('django.db.models.fields.related.OneToOneField')(related_name='meta', unique=True, null=True, on_delete=models.SET_NULL, to=orm['vhmdb.Vhost'])),
            ('database', self.gf('django.db.models.fields.CharField')(max_length=64, null=True)),
            ('group', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
            ('www_path', self.gf('django.db.models.fields.CharField')(max_length=255, null=True)),
        ))
        db.send_create_signal('vhmdb', ['VhostMeta'])


    def backwards(self, orm):
        # Deleting model 'SitePackage'
        db.delete_table('vhm_package')

        # Deleting model 'VhostRent'
        db.delete_table('vhm_vhost_rent')

        # Deleting model 'VhostRentMeta'
        db.delete_table('vhm_vhost_rent_meta')

        # Deleting model 'User'
        db.delete_table('vhm_user')

        # Deleting model 'UserMeta'
        db.delete_table('vhm_user_meta')

        # Deleting model 'UserGroup'
        db.delete_table('vhm_user_group')

        # Removing M2M table for field users on 'UserGroup'
        db.delete_table(db.shorten_name('vhm_user_group_users'))

        # Deleting model 'Interface'
        db.delete_table('vhm_interface')

        # Deleting model 'Vhost'
        db.delete_table('vhm_vhost')

        # Deleting model 'VhostMeta'
        db.delete_table('vhm_vhost_meta')


    models = {
        'vhmdb.interface': {
            'Meta': {'object_name': 'Interface', 'db_table': "'vhm_interface'"},
            'device': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'gateway': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip_address': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '32'}),
            'netmask': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        },
        'vhmdb.sitepackage': {
            'Meta': {'object_name': 'SitePackage', 'db_table': "'vhm_package'"},
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'package': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'Z'", 'max_length': '1'})
        },
        'vhmdb.user': {
            'Meta': {'object_name': 'User', 'db_table': "'vhm_user'"},
            'admin': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date_created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'disabled': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_vhosts': ('django.db.models.fields.IntegerField', [], {'default': '-1'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '16'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '77'})
        },
        'vhmdb.usergroup': {
            'Meta': {'object_name': 'UserGroup', 'db_table': "'vhm_user_group'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'users': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['vhmdb.User']", 'symmetrical': 'False'})
        },
        'vhmdb.usermeta': {
            'Meta': {'object_name': 'UserMeta', 'db_table': "'vhm_user_meta'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'linux_password': ('django.db.models.fields.CharField', [], {'max_length': '13'}),
            'linux_user': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'mysql_password': ('django.db.models.fields.CharField', [], {'max_length': '41'}),
            'mysql_user': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'meta'", 'unique': 'True', 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['vhmdb.User']"})
        },
        'vhmdb.vhost': {
            'Meta': {'object_name': 'Vhost', 'db_table': "'vhm_vhost'"},
            'allow_override': ('django.db.models.fields.TextField', [], {'default': "'All'"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'interface': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vhmdb.Interface']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'}),
            'options': ('django.db.models.fields.TextField', [], {'default': "'All'"}),
            'port': ('django.db.models.fields.PositiveIntegerField', [], {'default': '80'})
        },
        'vhmdb.vhostmeta': {
            'Meta': {'object_name': 'VhostMeta', 'db_table': "'vhm_vhost_meta'"},
            'database': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'group': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vhost': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'meta'", 'unique': 'True', 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['vhmdb.Vhost']"}),
            'www_path': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        },
        'vhmdb.vhostrent': {
            'Meta': {'object_name': 'VhostRent', 'db_table': "'vhm_vhost_rent'"},
            'end_date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vhmdb.SitePackage']", 'null': 'True', 'blank': 'True'}),
            'start_date': ('django.db.models.fields.DateTimeField', [], {}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'P'", 'max_length': '1'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vhmdb.User']"}),
            'vhost': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['vhmdb.Vhost']"})
        },
        'vhmdb.vhostrentmeta': {
            'Meta': {'object_name': 'VhostRentMeta', 'db_table': "'vhm_vhost_rent_meta'"},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'linux_group': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'linux_user': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'mysql_db': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True'}),
            'mysql_user': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True'}),
            'rent': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'meta'", 'unique': 'True', 'null': 'True', 'on_delete': 'models.SET_NULL', 'to': "orm['vhmdb.VhostRent']"}),
            'symlink': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'www_path': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'})
        }
    }

    complete_apps = ['vhmdb']