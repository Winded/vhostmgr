
user = angular.module "vhmUser", ["vhmConstants", "vhmConfig", "restangular", "vhmTable"]

class UserTableController extends TableController
    @register user
    @inject "$scope", "Restangular", "vhmc"

    initialize: ->
        super()

        @fields
            name:
                position: 1
                label: @gettext("Username")
                width: "20%"
            display_name:
                position: 2
                label: @gettext("Display name")
                width: "20%"
            max_vhosts:
                position: 3
                label: @gettext("Rent limit")
                width: "20%"
            status:
                position: 4
                label: @gettext("Status")
                width: "20%"
            date_created:
                position: 5
                label: @gettext("Creation date")
                width: "20%"

        @s.top_links = [
            href: @vhmc.urls.users.create, icon: "glyphicon-plus", label: @gettext("Create user"),
                href: @vhmc.urls.users.group_create, icon: "glyphicon-list-alt", label: @gettext("Create users with namelist"),
        ]

        @s.table.orderBy = "name"

        users = @Restangular.all("users")
        @loadDataToTable(users.getList())