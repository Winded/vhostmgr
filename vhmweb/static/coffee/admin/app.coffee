
admin = angular.module "vhmAdmin", ["vhmConstants", "restangular", "vhmConfig", 
	"vhmProfile", "vhmRent", "vhmRentQuick", "vhmVhost", "ngRoute"]

admin.config ($routeProvider, vhmc, gettext) ->
	$routeProvider
		.when "/",
			templateUrl: "#{vhmc.static_prefix}templates/admin_index.html"
			controller: "IndexController"
			resolve:
				title: ($rootScope) -> 
					$rootScope.title = gettext("Administration")
					$rootScope.subtitle = gettext("Welcome")
					vhmc.user.then (user) ->
						$rootScope.subtitle = gettext("Welcome") + ", #{user.display_name}"
		.otherwise
			redirectTo: "/"

class IndexController extends BaseCtrl
	@register admin
	@inject "$scope", "$rootScope", "vhmc", "gettext", "Restangular"

	initialize: ->

		super()

		@s.links = [
			href: "#/rents/quick/", icon: "glyphicon-time", label: @gettext("Quick rent"),
				href: "#/profile/", icon: "glyphicon-user", label: @gettext("My profile"),
			href: "#/rents/", icon: "glyphicon-time", label: @gettext("Rents"),
				href: "/logout/", icon: "glyphicon-log-out", label: @gettext("Logout"),
		]