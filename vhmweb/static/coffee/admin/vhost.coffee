
vhost = angular.module "vhmVhost", ["vhmConstants", "vhmConfig", "restangular", "vhmTable"]

class VhostTableController extends TableController
    @register vhost
    @inject "$scope", "Restangular", "vhmc"

    initialize: ->
        super()

        @fields
            name:
                position: 1
                label: @gettext("Name")
                width: "180px"
            endpoint:
                position: 2
                label: @gettext("Endpoint")
                width: "180px"
            options:
                position: 3
                label: @gettext("Options")
                width: "180px"
            allow_override:
                position: 3
                label: @gettext("Override setting")
                width: "230px"
            is_active:
                position: 4
                label: @gettext("In use")
                width: "120px"

        @s.top_links = [
            href: @vhmc.urls.vhosts.create, icon: "glyphicon-plus", label: @gettext("Create VirtualHost"),
                href: @vhmc.urls.vhosts.group_create, icon: "glyphicon-list-alt", label: @gettext("Create numbered VirtualHosts"),
        ]

        @s.table.orderBy = "endpoint"

        vhosts = @Restangular.all("vhosts")
        @loadDataToTable(vhosts.getList())