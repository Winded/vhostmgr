
startFrom = angular.module "startFrom", []

startFrom.filter "startFrom", -> (input, start) ->
    start = +start
    return input.slice(start)