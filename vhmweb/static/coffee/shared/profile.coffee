
profile = angular.module "vhmProfile", ["vhmConstants", "ngRoute"]


profile.config ($routeProvider, vhmc, gettext) ->
    $routeProvider
        .when "/profile/",
            templateUrl: "#{vhmc.static_prefix}templates/profile.html"
            controller: "ProfileController"
            resolve:
                title: ($rootScope) -> 
                    $rootScope.title = $rootScope.subtitle = gettext("Profile")
        .when "/profile/change_password/",
            templateUrl: "#{vhmc.static_prefix}templates/form.html"
            controller: "ChangePasswordController"
            resolve:
                title: ($rootScope) -> 
                    $rootScope.title = $rootScope.subtitle = gettext("Change password")


class ProfileController extends BaseCtrl
    @register profile
    @inject "$scope", "gettext", "vhmc", "Restangular"

    initialize: ->

        super()

        @s.loading = true

        @Restangular.oneUrl("user", "/api/shared/user/").get().then (data) =>
            @s.user_info = [
                label: @gettext("Username"), value: data.name,
                    label: @gettext("Display name"), value: data.display_name,
                label: @gettext("Status"), value: data.status_display,
                    label: @gettext("Active rents"), value: data.active_rents,
                label: @gettext("Rent limit"), value: data.max_vhosts_display,
                    label: @gettext("Creation date"), value: data.date_created_display,
            ]
            @s.loading = false


class ChangePasswordController extends FormController
    @register profile
    @inject "$scope", "$rootScope", "$location", "gettext", "Restangular"

    initialize: ->
        super()

        @fields
            password:
                label: @gettext("New password")
                type: "password"
                required: true
            password_repeat:
                label: @gettext("Retype new password")
                type: "password"
                required: true

        @s.return_link = "/profile/"
        @s.submit_text = @gettext("Change password")

    validate: ->
        super()
        if @s.data.password != @s.data.password_repeat
            @add_error("__all__", @gettext("Passwords do not match."))

    submit: ->
        valid = super()
        if not valid
            return

        @s.submitting = true

        promise = @Restangular.oneUrl("user/set_password", "/api/shared/user/set_password/")
            .post(null, password: @s.data.password)
        promise.then (data) =>
            @s.add_message(@gettext("Password changed."), "success")
            @$location.path(@s.last_route or @s.return_link)
        , (response) =>
            @s.errors = response.data
            @s.submitting = false