

dates = angular.module "jsonDates", ["restangular"]
dates.config (RestangularProvider) ->
    RestangularProvider.setOnElemRestangularized (elem, isCollection) ->
        if isCollection then return elem
        pattern = /^([d]{4})-([d]{2})-([d]{2})T([d]{2}):([d]{2}):([d]{2})(Z|(?:[+-][d]{2}[:]?[d]{2}))$/
        for k, v of elem
            if pattern.test(v)
                dv = new Date(Date.parse(v))
                elem[k] = dv
        return elem



@addMinutes = (date, minutes) ->
	return new Date(date.getTime() + minutes*60000)