
rent_quick = angular.module "vhmRentQuick", ["vhmConstants", "vhmForm", "restangular", "ngRoute"]


rent_quick.config ($routeProvider, vhmc, gettext) ->
    $routeProvider
        .when "/rents/quick/"
            templateUrl: "#{vhmc.static_prefix}templates/form.html"
            controller: "RentQuickController"
            resolve:
                title: ($rootScope) -> 
                    $rootScope.title = $rootScope.subtitle = gettext("Quick rent")


class RentQuickController extends FormController
    @register rent_quick
    @inject "$scope", "$rootScope", "gettext", "Restangular", "$location"

    initialize: ->
        super()

        @resolved_data = {}

        @fields
            time:
                label: @gettext("Rent time")
                type: "select"
                default: 15
                choices: [
                    label: "15 " + @gettext("minutes"), value: 15,
                        label: "30 " + @gettext("minutes"), value: 30,
                    label: "1 " + @gettext("hour"), value: 60,
                        label: "2 " + @gettext("hours"), value: 120,
                ]
            package:
                label: @gettext("Package")
                type: "select"
                null_label: @gettext("No package")
                choices: []

        @s.return_link = "/rents/"
        @s.submit_text = @gettext("Rent")

    resolve_packages: =>
        promise = @Restangular.allUrl("packages/options", "/api/shared/packages/options/").getList()
        promise.then (data) =>
            @set_field_choices("package", data)
        return promise

    submit: =>
        valid = super()
        if not valid
            return

        @s.submitting = true
        vhostsav = @Restangular.oneUrl("vhosts/get_available", "/api/shared/vhosts/get_available/")
        rents = @Restangular.allUrl("rents", "/api/student/rents/")

        start_date = addMinutes(new Date(), 1)
        end_date = addMinutes(start_date, @s.data.time)

        if not @resolved_data.vhost?
            promise = vhostsav.get 
                start_date: start_date.toISOString(),
                end_date: end_date.toISOString()
            promise.then (data) =>
                @resolved_data.vhost = data.id
                @submit()
            , (response) =>
                @s.errors["__all__"] = [response.data.detail]
                @s.submitting = false
            return

        @resolved_data.start_date = start_date
        @resolved_data.end_date = end_date
        @resolved_data.package = @s.data.package

        promise = rents.post(@resolved_data)
        promise.then (data) =>
            @s.add_message(@gettext("Rent added."), "success")
            @$location.path(@s.last_route or @s.return_link)
        , (response) =>
            @s.errors = response.data
            @s.submitting = false