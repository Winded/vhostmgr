
table = angular.module "vhmTable", ["ui.bootstrap", "startFrom", "vhmConstants"]

table.run (paginationConfig, gettext) ->
    paginationConfig.firstText = gettext("First")
    paginationConfig.lastText = gettext("Last")
    paginationConfig.nextText = gettext("Next")
    paginationConfig.previousText = gettext("Previous")
    paginationConfig.itemsPerPage = 15

class @TableController extends BaseCtrl
    @inject "$scope"

    initialize: ->
        super()
        @s.loaded = false

        @s.table = {}
        @s.table.fields = []
        @s.table.buttons = []
        @s.table.buttonFieldWidth = "24px"

        # Pagination setup
        @s.showPagination = true
        @s.showPagesMax = 5
        @s.currentPage = 1
        @s.itemsPerPage = 15

        @s.table.getValue = @tableGetValue
        @s.table.getDisplay = @tableGetDisplay
        @s.table.isNull = @tableIsNull
        @s.table.reOrder = @tableOrderBy
        @s.table.isOrderedBy = @tableIsOrderedBy
        @s.table.hasLink = @tableHasLink
        @s.table.getLink = @tableGetLink

    tableGetValue: (field, row) =>
        value = row[field.name]
        if not value? and field.default?
            return field.default
        else if not value?
            return null
        return value

    tableGetDisplay: (field, row) =>
        value = row["#{field.name}_display"]
        if not value?
            return @tableGetValue(field, row)
        return value

    tableIsNull: (field, row) =>
        return not row[field.name]?

    tableOrderBy: (field) =>
        if @s.table.orderBy == field.name
            @s.table.orderBy = "-#{field.name}"
        else
            @s.table.orderBy = field.name

    tableIsOrderedBy: (field, negative) =>
        if not negative and @s.table.orderBy == field.name
            return true
        else if negative and @s.table.orderBy == "-#{field.name}"
            return true
        else
            return false

    tableHasLink: (field, row) =>
        row["#{field.name}_link"]?

    tableGetLink: (field, row) =>
        row["#{field.name}_link"]

    loadDataToTable: (promise) ->
        promise.then (data) =>
            @s.table.rows = data
            @s.loaded = true

    getPageStart: =>
        return (@s.itemsPerPage - 1) * (@s.currentPage - 1)

    setPage: (page) =>
        @s.currentPage = page

    fields: (data) ->
        fields = []
        for name, value of data
            obj = {}
            obj.name = name
            for k, v of value
                obj[k] = v

            fields.push(obj)
        @s.table.fields = fields

    button: (title, icon, href, args=null, condition=null, target="_self", is_global=false) ->
        if not args?
            args = (row) -> {}
        if not condition?
            condition = (row) -> true
        prefix = if is_global then "" else "#"
        btn =
            title: title
            icon: icon
            href: if href? then "#{prefix}#{href}" else "javascript:void(0)"
            args: args
            target: target
            condition: condition
        @s.table.buttons.push(btn)