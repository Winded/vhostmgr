# http://www.devign.me/angular-dot-js-coffeescript-controller-base-class

class @BaseCtrl
    @register: (app, name) ->
        name ?= @name || @toString().match(/function\s*(.*?)\(/)?[1]
        app.controller name, @

    @inject: (args...) ->
        if not @$inject?
            @$inject = []
        @$inject = @$inject.concat(args)

    constructor: (args...) ->
        for key, index in @constructor.$inject
            @[key] = args[index]
        if @$scope?
            for key, index in @constructor.$inject
                @$scope[key] = args[index] unless key == "$scope"

        for key, fn of @constructor.prototype
            continue unless typeof fn is 'function'
            continue if key in ['constructor', 'initialize'] or key[0] is '_'
            @$scope[key] = fn.bind?(@) || _.bind(fn, @)

        @initialize?()

    initialize: ->
        @s = @$scope if @$scope?