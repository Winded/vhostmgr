
form = angular.module "vhmForm", ["restangular", "ui.bootstrap", "ui.bootstrap.datetimepicker", "ngRoute"]

form.run (dateTimePickerConfig) ->
    dateTimePickerConfig.weekStart = 1

class @FormController extends BaseCtrl
    @inject "$scope", "$routeParams"

    initialize: ->
        super()

        @s.fields = []
        @s.errors = {}
        @s.data = {}
        @s.submit_text = "Submit"

        @s.loading = true
        @resolve_count = 0
        @resolve_total = 0
        for name, obj of @s
            continue unless typeof obj is 'function'
            continue if name in ['constructor', 'initialize'] or name[0] is '_'
            continue if name[..7] != "resolve_"
            @resolve_total++
            promise = obj()
            promise.then () =>
                @resolve_count++
                if @resolve_count == @resolve_total
                    @onResolved()
                    @s.loading = false
        if @resolve_total == 0
            @onResolved()
            @s.loading = false

    fields: (data) ->
        fields = []
        for name, value of data
            obj = {}
            obj.name = name
            for k, v of value
                obj[k] = v

            fields.push(obj)
        @s.fields = fields
        for f in fields
            @s.data[f.name] = if f.default? then f.default else null

    set_field_choices: (field, choices) ->
        field_obj = _.where(@s.fields, (f) -> f.name == field)[0]
        field_obj.choices = choices

    add_error: (field, message) =>
        if not @s.errors[field]?
            @s.errors[field] = []
        @s.errors[field].push(message)
        @s.has_error = true

    clear_errors: =>
        @s.errors = []
        @s.has_error = false

    onResolved: =>
        for f in @s.fields
            if @$routeParams[f.name]?
                val = @$routeParams[f.name]
                if not isNaN(parseFloat(val))
                    val = parseFloat(val)
                @s.data[f.name] = val

    validate: ->
        for f in @s.fields
            if f.required? and (not @s.data[f.name]? or @s.data[f.name].length == 0)
                @add_error(f.name, @gettext("This field is required."))

    submit: ->
        @clear_errors()

        @validate()
        if @s.has_error
            return false

        return true