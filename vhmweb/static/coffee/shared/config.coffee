
cfg = angular.module "vhmConfig", ["vhmConstants", "restangular", "ngCookies"]

cfg.config ($locationProvider) ->
    # This would look nicer, but it's most propably going to cause problems in future development
    #$locationProvider.html5Mode(true)

cfg.run ($http, $cookies, $rootScope, $location, $templateCache, Restangular, vhmc) ->

    $http.defaults.headers.post["X-CSRFToken"] = $cookies.csrftoken
    $http.defaults.xsrfCookieName = "csrftoken"
    $http.defaults.xsrfHeaderName = "X-CSRFToken"

    Restangular.setBaseUrl(vhmc.baseUrl)
    Restangular.setRequestSuffix("/")
    Restangular.setRestangularFields(selfLink: "link")

    $rootScope.message = null
    $rootScope.add_message = (text, tag) ->
        $rootScope.message = text: text, tag: tag
    $rootScope.clear_message = ->
        $rootScope.message = null

    $rootScope.urlFormat = (url, params) ->
        result = url
        for name, value of params
            result = result.replace(":" + name, value)
        return result

    $rootScope.$on "$routeChangeSuccess", (event, newUrl, oldUrl) ->
        if oldUrl?
            $rootScope.last_route = oldUrl.originalPath

    moment.lang(vhmc.language)

    vhmc.user = Restangular.one("user").get()

    # Precache templates
    $http.get("#{vhmc.static_prefix}templates/admin_index.html", cache: $templateCache)
    $http.get("#{vhmc.static_prefix}templates/delete.html", cache: $templateCache)
    $http.get("#{vhmc.static_prefix}templates/form.html", cache: $templateCache)
    $http.get("#{vhmc.static_prefix}templates/profile.html", cache: $templateCache)
    $http.get("#{vhmc.static_prefix}templates/student_index.html", cache: $templateCache)
    $http.get("#{vhmc.static_prefix}templates/table.html", cache: $templateCache)