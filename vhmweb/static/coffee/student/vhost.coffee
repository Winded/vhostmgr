
vhost = angular.module "vhmVhost", ["vhmConstants", "vhmConfig", "ngRoute", "restangular", "vhmTable"]

vhost.config ($routeProvider, vhmc, gettext) ->
    $routeProvider
        .when "/vhosts/",
            templateUrl: "#{vhmc.static_prefix}templates/table.html"
            controller: "VhostTableController"
            resolve:
                title: ($rootScope) -> 
                    $rootScope.title = $rootScope.subtitle = gettext("VirtualHosts")

class VhostTableController extends TableController
    @register vhost
    @inject "$scope", "$location", "Restangular", "gettext", "$sce"

    initialize: ->
        super()

        @fields
            name:
                position: 1
                label: @gettext("Name")
                width: "15%"
            endpoint:
                position: 2
                label: @gettext("Endpoint")
                width: "15%"
            options:
                position: 3
                label: @gettext("Options")
                width: "15%"
            allow_override:
                position: 3
                label: @gettext("Override setting")
                width: "20%"
            is_active:
                position: 4
                label: @gettext("In use")
                width: "15%"

        @button @gettext("Rent"), "glyphicon-plus", "/rents/new/?vhost=:id", (row) ->
            {id: row.id}
        , (row) -> true

        @s.showPagination = false
        @s.itemsPerPage = 1000

        @s.table.orderBy = "endpoint"
        @s.table.buttonFieldWidth = "10%"

        vhosts = @Restangular.all("vhosts")
        @loadDataToTable(vhosts.getList())