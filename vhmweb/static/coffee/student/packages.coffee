
packages = angular.module "vhmPackage", ["vhmConstants", "vhmConfig", "ngRoute", "restangular", "vhmTable"]

packages.config ($routeProvider, vhmc, gettext) ->
    $routeProvider
        .when "/packages/",
            templateUrl: "#{vhmc.static_prefix}templates/table.html"
            controller: "PackageTableController"
            resolve:
                title: ($rootScope) -> 
                    $rootScope.title = $rootScope.subtitle = gettext("Site packages")

class PackageTableController extends TableController
    @register packages
    @inject "$scope", "Restangular", "vhmc", "gettext", "$sce"

    initialize: ->
        super()

        @fields
            name:
                position: 1
                label: @gettext("Name")
                width: "20%"
            description:
                position: 2
                label: @gettext("Description")
                width: "50%"
            size:
                position: 3
                label: @gettext("File size")
                width: "20%"

        @button @gettext("Rent"), "glyphicon-plus", "/rents/new/?package=:id", (row) ->
            {id: row.id}
        , (row) -> true

        @s.showPagination = false
        @s.itemsPerPage = 1000000

        @s.table.orderBy = "name"
        @s.table.buttonFieldWidth = "10%"

        packages = @Restangular.all("packages")
        @loadDataToTable(packages.getList())