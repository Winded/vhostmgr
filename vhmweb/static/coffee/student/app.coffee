
student = angular.module "vhmStudent", ["vhmConstants", "restangular", "vhmConfig", 
	"vhmProfile", "vhmRent", "vhmRentQuick", "vhmVhost", "vhmPackage", "vhmProfile", "ngRoute"]

student.config ($routeProvider, vhmc) ->
	$routeProvider
		.when "/",
			templateUrl: "#{vhmc.static_prefix}templates/student_index.html"
			controller: "IndexController"
			resolve:
				title: ($rootScope) -> 
					$rootScope.title = gettext("Front page")
					$rootScope.subtitle = gettext("Welcome")
					vhmc.user.then (user) ->
						$rootScope.subtitle = gettext("Welcome") + ", #{user.display_name}"
		.otherwise
			redirectTo: "/"

class IndexController extends BaseCtrl
	@register student
	@inject "$scope", "$rootScope", "vhmc", "gettext", "Restangular"

	initialize: ->

		super()

		@s.links = [
			href: "#/rents/quick/", icon: "glyphicon-time", label: @gettext("Quick rent"),
				href: "#/profile/", icon: "glyphicon-user", label: @gettext("My profile"),
			href: "#/help/", icon: "glyphicon-flag", label: @gettext("Help"),
				href: "/logout/", icon: "glyphicon-log-out", label: @gettext("Logout"),
		]