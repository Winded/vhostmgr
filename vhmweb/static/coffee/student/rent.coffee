
STATUS_CURRENT = ["P", "W", "A"]


rent = angular.module "vhmRent", ["vhmConstants", "vhmForm", "restangular", "vhmTable", "ui.bootstrap.datetimepicker", "ngRoute"]


rent.config ($routeProvider, vhmc, gettext) ->
    $routeProvider
        .when "/rents/",
            templateUrl: "#{vhmc.static_prefix}templates/table.html"
            controller: "RentTableController"
            resolve:
                title: ($rootScope) -> 
                    $rootScope.title = $rootScope.subtitle = gettext("Rents")
        .when "/rents/new/"
            templateUrl: "#{vhmc.static_prefix}templates/form.html"
            controller: "RentCreateController"
            resolve:
                title: ($rootScope) -> 
                    $rootScope.title = $rootScope.subtitle = gettext("New rent")
        .when "/rents/:rent_id/cancel/"
            templateUrl: "#{vhmc.static_prefix}templates/delete.html"
            controller: "RentCancelController"
            resolve:
                title: ($rootScope) -> 
                    $rootScope.title = $rootScope.subtitle = gettext("Cancel rent")


class RentTableController extends TableController
    @register rent
    @inject "$scope", "$location", "Restangular", "gettext"

    initialize: ->
        super()

        @fields
            vhost:
                position: 1
                label: @gettext("VirtualHost")
                width: "15%"
            start_date:
                position: 2
                label: @gettext("Start date")
                width: "15%"
            end_date:
                position: 3
                label: @gettext("End date")
                width: "15%"
            package:
                position: 4
                label: @gettext("Package")
                default: @gettext("No package")
                width: "15%"
            status:
                position: 5
                label: @gettext("Status")
                width: "15%"

        @s.top_links = [
            href: "/rents/new/", icon: "glyphicon-plus", label: @gettext("New rent")
        ]

        @button @gettext("Cancel"), "glyphicon-remove", "/rents/:id/cancel/", (row) ->
                {id: row.id}
            ,(row) ->
                row.status in STATUS_CURRENT

        @button @gettext("View site"), "glyphicon-share-alt", "/redirect/:id/", (row) ->
                {id: row.vhost}
            ,(row) -> 
                row.status == "A"
            , "_blank", true

        @s.table.buttonFieldWidth = "10%"

        @s.table.orderBy = "-start_date"

        rents = @Restangular.all("rents")
        @loadDataToTable(rents.getList())


class RentCreateController extends FormController
    @register rent
    @inject "$scope", "gettext", "vhmc", "Restangular", "$rootScope", "$location"

    initialize: ->
        super()

        @fields
            vhost:
                label: @gettext("VirtualHost")
                type: "select"
                null_label: @gettext("Any")
                choices: []
            start_date:
                label: @gettext("Start date")
                type: "datetime"
                default: addMinutes(new Date(), 5)
            end_date:
                label: @gettext("End date")
                type: "datetime"
                default: addMinutes(new Date(), 65)
            package:
                label: @gettext("Package")
                type: "select"
                null_label: @gettext("No package")
                choices: []

        @s.return_link = "/rents/"
        @s.submit_text = @gettext("Rent")

    resolve_vhosts: =>
        promise = @Restangular.allUrl("vhosts/options", "/api/shared/vhosts/options/").getList()
        promise.then (data) =>
            @set_field_choices("vhost", data)
        return promise

    resolve_packages: =>
        promise = @Restangular.allUrl("packages/options", "/api/shared/packages/options/").getList()
        promise.then (data) =>
            @set_field_choices("package", data)
        return promise

    validate: ->
        super()

        now = new Date()
        if @s.data.start_date <= now
            @add_error("start_date", @gettext("Start date/time must be later than current date/time."))

        if @s.data.start_date >= @s.data.end_date
            @add_error("start_date", @gettext("Start date/time must be less than end date/time."))

    submit: =>
        valid = super()
        if not valid
            return

        @s.submitting = true
        vhostsav = @Restangular.oneUrl("vhosts/get_available", "/api/shared/vhosts/get_available/")
        rents = @Restangular.all("rents")

        if not @s.data.vhost?
            promise = vhostsav.get
                start_date: @s.data.start_date.toISOString(),
                end_date: @s.data.end_date.toISOString()
            promise.then (data) =>
                @s.data.vhost = data.id
                @submit()
            , (response) =>
                @add_error("__all__", response.data.detail)
                @s.submitting = false
            return

        promise = rents.post(@s.data)
        promise.then (data) =>
            @s.add_message(@gettext("Rent added."), "success")
            @$location.path(@s.last_route or @s.return_link)
        , (response) =>
            @s.errors = response.data
            @s.submitting = false


class RentCancelController extends BaseCtrl
    @register rent
    @inject "$scope", "$rootScope", "$routeParams", "$location", "vhmc", "gettext", "Restangular"

    initialize: ->
        super()
        @s.text = @gettext("Are you sure you want to cancel this rent?")
        @s.return_link = "/rents/"

        @rRent = @Restangular.one("rents", @$routeParams.rent_id)
        @rRent.get().then (data) =>
            @rent = data
            @$rootScope.subtitle = @gettext("Cancel rent") + ": #{data.vhost_display} (ID: #{data.id})"

    delete: =>
        @s.deleting = true
        @rRent.one("cancel").post().then () =>
            @s.deleting = false
            @s.add_message(@gettext("Rent cancelled."), "success")
            @$location.path(@s.last_route or @s.return_link)
        , (response) =>
            @s.deleting = false
            @s.add_message(@gettext("An error occurred. Please try again later."), "danger")
            @$location.path(@s.last_route or @s.return_link)