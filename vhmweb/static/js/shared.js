// Generated by CoffeeScript 1.4.0
(function() {
  var ChangePasswordController, ProfileController, RentQuickController, cfg, dates, form, profile, rent_quick, startFrom, table,
    __slice = [].slice,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.BaseCtrl = (function() {

    BaseCtrl.register = function(app, name) {
      var _ref;
      if (name == null) {
        name = this.name || ((_ref = this.toString().match(/function\s*(.*?)\(/)) != null ? _ref[1] : void 0);
      }
      return app.controller(name, this);
    };

    BaseCtrl.inject = function() {
      var args;
      args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      if (!(this.$inject != null)) {
        this.$inject = [];
      }
      return this.$inject = this.$inject.concat(args);
    };

    function BaseCtrl() {
      var args, fn, index, key, _i, _j, _len, _len1, _ref, _ref1, _ref2;
      args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      _ref = this.constructor.$inject;
      for (index = _i = 0, _len = _ref.length; _i < _len; index = ++_i) {
        key = _ref[index];
        this[key] = args[index];
      }
      if (this.$scope != null) {
        _ref1 = this.constructor.$inject;
        for (index = _j = 0, _len1 = _ref1.length; _j < _len1; index = ++_j) {
          key = _ref1[index];
          if (key !== "$scope") {
            this.$scope[key] = args[index];
          }
        }
      }
      _ref2 = this.constructor.prototype;
      for (key in _ref2) {
        fn = _ref2[key];
        if (typeof fn !== 'function') {
          continue;
        }
        if ((key === 'constructor' || key === 'initialize') || key[0] === '_') {
          continue;
        }
        this.$scope[key] = (typeof fn.bind === "function" ? fn.bind(this) : void 0) || _.bind(fn, this);
      }
      if (typeof this.initialize === "function") {
        this.initialize();
      }
    }

    BaseCtrl.prototype.initialize = function() {
      if (this.$scope != null) {
        return this.s = this.$scope;
      }
    };

    return BaseCtrl;

  })();

  cfg = angular.module("vhmConfig", ["vhmConstants", "restangular", "ngCookies"]);

  cfg.config(function($locationProvider) {});

  cfg.run(function($http, $cookies, $rootScope, $location, $templateCache, Restangular, vhmc) {
    $http.defaults.headers.post["X-CSRFToken"] = $cookies.csrftoken;
    $http.defaults.xsrfCookieName = "csrftoken";
    $http.defaults.xsrfHeaderName = "X-CSRFToken";
    Restangular.setBaseUrl(vhmc.baseUrl);
    Restangular.setRequestSuffix("/");
    Restangular.setRestangularFields({
      selfLink: "link"
    });
    $rootScope.message = null;
    $rootScope.add_message = function(text, tag) {
      return $rootScope.message = {
        text: text,
        tag: tag
      };
    };
    $rootScope.clear_message = function() {
      return $rootScope.message = null;
    };
    $rootScope.urlFormat = function(url, params) {
      var name, result, value;
      result = url;
      for (name in params) {
        value = params[name];
        result = result.replace(":" + name, value);
      }
      return result;
    };
    $rootScope.$on("$routeChangeSuccess", function(event, newUrl, oldUrl) {
      if (oldUrl != null) {
        return $rootScope.last_route = oldUrl.originalPath;
      }
    });
    moment.lang(vhmc.language);
    vhmc.user = Restangular.one("user").get();
    $http.get("" + vhmc.static_prefix + "templates/admin_index.html", {
      cache: $templateCache
    });
    $http.get("" + vhmc.static_prefix + "templates/delete.html", {
      cache: $templateCache
    });
    $http.get("" + vhmc.static_prefix + "templates/form.html", {
      cache: $templateCache
    });
    $http.get("" + vhmc.static_prefix + "templates/profile.html", {
      cache: $templateCache
    });
    $http.get("" + vhmc.static_prefix + "templates/student_index.html", {
      cache: $templateCache
    });
    return $http.get("" + vhmc.static_prefix + "templates/table.html", {
      cache: $templateCache
    });
  });

  dates = angular.module("jsonDates", ["restangular"]);

  dates.config(function(RestangularProvider) {
    return RestangularProvider.setOnElemRestangularized(function(elem, isCollection) {
      var dv, k, pattern, v;
      if (isCollection) {
        return elem;
      }
      pattern = /^([d]{4})-([d]{2})-([d]{2})T([d]{2}):([d]{2}):([d]{2})(Z|(?:[+-][d]{2}[:]?[d]{2}))$/;
      for (k in elem) {
        v = elem[k];
        if (pattern.test(v)) {
          dv = new Date(Date.parse(v));
          elem[k] = dv;
        }
      }
      return elem;
    });
  });

  this.addMinutes = function(date, minutes) {
    return new Date(date.getTime() + minutes * 60000);
  };

  form = angular.module("vhmForm", ["restangular", "ui.bootstrap", "ui.bootstrap.datetimepicker", "ngRoute"]);

  form.run(function(dateTimePickerConfig) {
    return dateTimePickerConfig.weekStart = 1;
  });

  this.FormController = (function(_super) {

    __extends(FormController, _super);

    function FormController() {
      this.onResolved = __bind(this.onResolved, this);

      this.clear_errors = __bind(this.clear_errors, this);

      this.add_error = __bind(this.add_error, this);
      return FormController.__super__.constructor.apply(this, arguments);
    }

    FormController.inject("$scope", "$routeParams");

    FormController.prototype.initialize = function() {
      var name, obj, promise, _ref,
        _this = this;
      FormController.__super__.initialize.call(this);
      this.s.fields = [];
      this.s.errors = {};
      this.s.data = {};
      this.s.submit_text = "Submit";
      this.s.loading = true;
      this.resolve_count = 0;
      this.resolve_total = 0;
      _ref = this.s;
      for (name in _ref) {
        obj = _ref[name];
        if (typeof obj !== 'function') {
          continue;
        }
        if ((name === 'constructor' || name === 'initialize') || name[0] === '_') {
          continue;
        }
        if (name.slice(0, 8) !== "resolve_") {
          continue;
        }
        this.resolve_total++;
        promise = obj();
        promise.then(function() {
          _this.resolve_count++;
          if (_this.resolve_count === _this.resolve_total) {
            _this.onResolved();
            return _this.s.loading = false;
          }
        });
      }
      if (this.resolve_total === 0) {
        this.onResolved();
        return this.s.loading = false;
      }
    };

    FormController.prototype.fields = function(data) {
      var f, fields, k, name, obj, v, value, _i, _len, _results;
      fields = [];
      for (name in data) {
        value = data[name];
        obj = {};
        obj.name = name;
        for (k in value) {
          v = value[k];
          obj[k] = v;
        }
        fields.push(obj);
      }
      this.s.fields = fields;
      _results = [];
      for (_i = 0, _len = fields.length; _i < _len; _i++) {
        f = fields[_i];
        _results.push(this.s.data[f.name] = f["default"] != null ? f["default"] : null);
      }
      return _results;
    };

    FormController.prototype.set_field_choices = function(field, choices) {
      var field_obj;
      field_obj = _.where(this.s.fields, function(f) {
        return f.name === field;
      })[0];
      return field_obj.choices = choices;
    };

    FormController.prototype.add_error = function(field, message) {
      if (!(this.s.errors[field] != null)) {
        this.s.errors[field] = [];
      }
      this.s.errors[field].push(message);
      return this.s.has_error = true;
    };

    FormController.prototype.clear_errors = function() {
      this.s.errors = [];
      return this.s.has_error = false;
    };

    FormController.prototype.onResolved = function() {
      var f, val, _i, _len, _ref, _results;
      _ref = this.s.fields;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        f = _ref[_i];
        if (this.$routeParams[f.name] != null) {
          val = this.$routeParams[f.name];
          if (!isNaN(parseFloat(val))) {
            val = parseFloat(val);
          }
          _results.push(this.s.data[f.name] = val);
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };

    FormController.prototype.validate = function() {
      var f, _i, _len, _ref, _results;
      _ref = this.s.fields;
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        f = _ref[_i];
        if ((f.required != null) && (!(this.s.data[f.name] != null) || this.s.data[f.name].length === 0)) {
          _results.push(this.add_error(f.name, this.gettext("This field is required.")));
        } else {
          _results.push(void 0);
        }
      }
      return _results;
    };

    FormController.prototype.submit = function() {
      this.clear_errors();
      this.validate();
      if (this.s.has_error) {
        return false;
      }
      return true;
    };

    return FormController;

  })(BaseCtrl);

  profile = angular.module("vhmProfile", ["vhmConstants", "ngRoute"]);

  profile.config(function($routeProvider, vhmc, gettext) {
    return $routeProvider.when("/profile/", {
      templateUrl: "" + vhmc.static_prefix + "templates/profile.html",
      controller: "ProfileController",
      resolve: {
        title: function($rootScope) {
          return $rootScope.title = $rootScope.subtitle = gettext("Profile");
        }
      }
    }).when("/profile/change_password/", {
      templateUrl: "" + vhmc.static_prefix + "templates/form.html",
      controller: "ChangePasswordController",
      resolve: {
        title: function($rootScope) {
          return $rootScope.title = $rootScope.subtitle = gettext("Change password");
        }
      }
    });
  });

  ProfileController = (function(_super) {

    __extends(ProfileController, _super);

    function ProfileController() {
      return ProfileController.__super__.constructor.apply(this, arguments);
    }

    ProfileController.register(profile);

    ProfileController.inject("$scope", "gettext", "vhmc", "Restangular");

    ProfileController.prototype.initialize = function() {
      var _this = this;
      ProfileController.__super__.initialize.call(this);
      this.s.loading = true;
      return this.Restangular.oneUrl("user", "/api/shared/user/").get().then(function(data) {
        _this.s.user_info = [
          {
            label: _this.gettext("Username"),
            value: data.name
          }, {
            label: _this.gettext("Display name"),
            value: data.display_name
          }, {
            label: _this.gettext("Status"),
            value: data.status_display
          }, {
            label: _this.gettext("Active rents"),
            value: data.active_rents
          }, {
            label: _this.gettext("Rent limit"),
            value: data.max_vhosts_display
          }, {
            label: _this.gettext("Creation date"),
            value: data.date_created_display
          }
        ];
        return _this.s.loading = false;
      });
    };

    return ProfileController;

  })(BaseCtrl);

  ChangePasswordController = (function(_super) {

    __extends(ChangePasswordController, _super);

    function ChangePasswordController() {
      return ChangePasswordController.__super__.constructor.apply(this, arguments);
    }

    ChangePasswordController.register(profile);

    ChangePasswordController.inject("$scope", "$rootScope", "$location", "gettext", "Restangular");

    ChangePasswordController.prototype.initialize = function() {
      ChangePasswordController.__super__.initialize.call(this);
      this.fields({
        password: {
          label: this.gettext("New password"),
          type: "password",
          required: true
        },
        password_repeat: {
          label: this.gettext("Retype new password"),
          type: "password",
          required: true
        }
      });
      this.s.return_link = "/profile/";
      return this.s.submit_text = this.gettext("Change password");
    };

    ChangePasswordController.prototype.validate = function() {
      ChangePasswordController.__super__.validate.call(this);
      if (this.s.data.password !== this.s.data.password_repeat) {
        return this.add_error("__all__", this.gettext("Passwords do not match."));
      }
    };

    ChangePasswordController.prototype.submit = function() {
      var promise, valid,
        _this = this;
      valid = ChangePasswordController.__super__.submit.call(this);
      if (!valid) {
        return;
      }
      this.s.submitting = true;
      promise = this.Restangular.oneUrl("user/set_password", "/api/shared/user/set_password/").post(null, {
        password: this.s.data.password
      });
      return promise.then(function(data) {
        _this.s.add_message(_this.gettext("Password changed."), "success");
        return _this.$location.path(_this.s.last_route || _this.s.return_link);
      }, function(response) {
        _this.s.errors = response.data;
        return _this.s.submitting = false;
      });
    };

    return ChangePasswordController;

  })(FormController);

  rent_quick = angular.module("vhmRentQuick", ["vhmConstants", "vhmForm", "restangular", "ngRoute"]);

  rent_quick.config(function($routeProvider, vhmc, gettext) {
    return $routeProvider.when("/rents/quick/", {
      templateUrl: "" + vhmc.static_prefix + "templates/form.html",
      controller: "RentQuickController",
      resolve: {
        title: function($rootScope) {
          return $rootScope.title = $rootScope.subtitle = gettext("Quick rent");
        }
      }
    });
  });

  RentQuickController = (function(_super) {

    __extends(RentQuickController, _super);

    function RentQuickController() {
      this.submit = __bind(this.submit, this);

      this.resolve_packages = __bind(this.resolve_packages, this);
      return RentQuickController.__super__.constructor.apply(this, arguments);
    }

    RentQuickController.register(rent_quick);

    RentQuickController.inject("$scope", "$rootScope", "gettext", "Restangular", "$location");

    RentQuickController.prototype.initialize = function() {
      RentQuickController.__super__.initialize.call(this);
      this.resolved_data = {};
      this.fields({
        time: {
          label: this.gettext("Rent time"),
          type: "select",
          "default": 15,
          choices: [
            {
              label: "15 " + this.gettext("minutes"),
              value: 15
            }, {
              label: "30 " + this.gettext("minutes"),
              value: 30
            }, {
              label: "1 " + this.gettext("hour"),
              value: 60
            }, {
              label: "2 " + this.gettext("hours"),
              value: 120
            }
          ]
        },
        "package": {
          label: this.gettext("Package"),
          type: "select",
          null_label: this.gettext("No package"),
          choices: []
        }
      });
      this.s.return_link = "/rents/";
      return this.s.submit_text = this.gettext("Rent");
    };

    RentQuickController.prototype.resolve_packages = function() {
      var promise,
        _this = this;
      promise = this.Restangular.allUrl("packages/options", "/api/shared/packages/options/").getList();
      promise.then(function(data) {
        return _this.set_field_choices("package", data);
      });
      return promise;
    };

    RentQuickController.prototype.submit = function() {
      var end_date, promise, rents, start_date, valid, vhostsav,
        _this = this;
      valid = RentQuickController.__super__.submit.call(this);
      if (!valid) {
        return;
      }
      this.s.submitting = true;
      vhostsav = this.Restangular.oneUrl("vhosts/get_available", "/api/shared/vhosts/get_available/");
      rents = this.Restangular.allUrl("rents", "/api/student/rents/");
      start_date = addMinutes(new Date(), 1);
      end_date = addMinutes(start_date, this.s.data.time);
      if (!(this.resolved_data.vhost != null)) {
        promise = vhostsav.get({
          start_date: start_date.toISOString(),
          end_date: end_date.toISOString()
        });
        promise.then(function(data) {
          _this.resolved_data.vhost = data.id;
          return _this.submit();
        }, function(response) {
          _this.s.errors["__all__"] = [response.data.detail];
          return _this.s.submitting = false;
        });
        return;
      }
      this.resolved_data.start_date = start_date;
      this.resolved_data.end_date = end_date;
      this.resolved_data["package"] = this.s.data["package"];
      promise = rents.post(this.resolved_data);
      return promise.then(function(data) {
        _this.s.add_message(_this.gettext("Rent added."), "success");
        return _this.$location.path(_this.s.last_route || _this.s.return_link);
      }, function(response) {
        _this.s.errors = response.data;
        return _this.s.submitting = false;
      });
    };

    return RentQuickController;

  })(FormController);

  startFrom = angular.module("startFrom", []);

  startFrom.filter("startFrom", function() {
    return function(input, start) {
      start = +start;
      return input.slice(start);
    };
  });

  table = angular.module("vhmTable", ["ui.bootstrap", "startFrom", "vhmConstants"]);

  table.run(function(paginationConfig, gettext) {
    paginationConfig.firstText = gettext("First");
    paginationConfig.lastText = gettext("Last");
    paginationConfig.nextText = gettext("Next");
    paginationConfig.previousText = gettext("Previous");
    return paginationConfig.itemsPerPage = 15;
  });

  this.TableController = (function(_super) {

    __extends(TableController, _super);

    function TableController() {
      this.setPage = __bind(this.setPage, this);

      this.getPageStart = __bind(this.getPageStart, this);

      this.tableGetLink = __bind(this.tableGetLink, this);

      this.tableHasLink = __bind(this.tableHasLink, this);

      this.tableIsOrderedBy = __bind(this.tableIsOrderedBy, this);

      this.tableOrderBy = __bind(this.tableOrderBy, this);

      this.tableIsNull = __bind(this.tableIsNull, this);

      this.tableGetDisplay = __bind(this.tableGetDisplay, this);

      this.tableGetValue = __bind(this.tableGetValue, this);
      return TableController.__super__.constructor.apply(this, arguments);
    }

    TableController.inject("$scope");

    TableController.prototype.initialize = function() {
      TableController.__super__.initialize.call(this);
      this.s.loaded = false;
      this.s.table = {};
      this.s.table.fields = [];
      this.s.table.buttons = [];
      this.s.table.buttonFieldWidth = "24px";
      this.s.showPagination = true;
      this.s.showPagesMax = 5;
      this.s.currentPage = 1;
      this.s.itemsPerPage = 15;
      this.s.table.getValue = this.tableGetValue;
      this.s.table.getDisplay = this.tableGetDisplay;
      this.s.table.isNull = this.tableIsNull;
      this.s.table.reOrder = this.tableOrderBy;
      this.s.table.isOrderedBy = this.tableIsOrderedBy;
      this.s.table.hasLink = this.tableHasLink;
      return this.s.table.getLink = this.tableGetLink;
    };

    TableController.prototype.tableGetValue = function(field, row) {
      var value;
      value = row[field.name];
      if (!(value != null) && (field["default"] != null)) {
        return field["default"];
      } else if (!(value != null)) {
        return null;
      }
      return value;
    };

    TableController.prototype.tableGetDisplay = function(field, row) {
      var value;
      value = row["" + field.name + "_display"];
      if (!(value != null)) {
        return this.tableGetValue(field, row);
      }
      return value;
    };

    TableController.prototype.tableIsNull = function(field, row) {
      return !(row[field.name] != null);
    };

    TableController.prototype.tableOrderBy = function(field) {
      if (this.s.table.orderBy === field.name) {
        return this.s.table.orderBy = "-" + field.name;
      } else {
        return this.s.table.orderBy = field.name;
      }
    };

    TableController.prototype.tableIsOrderedBy = function(field, negative) {
      if (!negative && this.s.table.orderBy === field.name) {
        return true;
      } else if (negative && this.s.table.orderBy === ("-" + field.name)) {
        return true;
      } else {
        return false;
      }
    };

    TableController.prototype.tableHasLink = function(field, row) {
      return row["" + field.name + "_link"] != null;
    };

    TableController.prototype.tableGetLink = function(field, row) {
      return row["" + field.name + "_link"];
    };

    TableController.prototype.loadDataToTable = function(promise) {
      var _this = this;
      return promise.then(function(data) {
        _this.s.table.rows = data;
        return _this.s.loaded = true;
      });
    };

    TableController.prototype.getPageStart = function() {
      return (this.s.itemsPerPage - 1) * (this.s.currentPage - 1);
    };

    TableController.prototype.setPage = function(page) {
      return this.s.currentPage = page;
    };

    TableController.prototype.fields = function(data) {
      var fields, k, name, obj, v, value;
      fields = [];
      for (name in data) {
        value = data[name];
        obj = {};
        obj.name = name;
        for (k in value) {
          v = value[k];
          obj[k] = v;
        }
        fields.push(obj);
      }
      return this.s.table.fields = fields;
    };

    TableController.prototype.button = function(title, icon, href, args, condition, target, is_global) {
      var btn, prefix;
      if (args == null) {
        args = null;
      }
      if (condition == null) {
        condition = null;
      }
      if (target == null) {
        target = "_self";
      }
      if (is_global == null) {
        is_global = false;
      }
      if (!(args != null)) {
        args = function(row) {
          return {};
        };
      }
      if (!(condition != null)) {
        condition = function(row) {
          return true;
        };
      }
      prefix = is_global ? "" : "#";
      btn = {
        title: title,
        icon: icon,
        href: href != null ? "" + prefix + href : "javascript:void(0)",
        args: args,
        target: target,
        condition: condition
      };
      return this.s.table.buttons.push(btn);
    };

    return TableController;

  })(BaseCtrl);

}).call(this);
