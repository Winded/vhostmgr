import os
from django.conf.urls import patterns, include, url
from django.views.i18n import javascript_catalog
from vhmweb import settings, views
from vhmweb.views import VhostRedirectView


js_info_dict = {
    "domain": "django",
    "packages": ("vhmweb", "django.conf")
}


urlpatterns = patterns('',

    url(r"^$", views.IndexView.as_view(), name="vhmmisc_index"),
    url(r"^login/$", views.LoginView.as_view(), name="vhmmisc_login"),
    url(r"^logout/$", views.LogoutView.as_view(), name="vhmmisc_logout"),

    url(r"^api/", include("vhmapi.urls")),

    url(r"^redirect/(?P<vhost>[a-zA-Z0-9]+)/$", VhostRedirectView.as_view(), name="vhmmisc_redirect"),

    url(r"^rosetta/", include("rosetta.urls")),
    url(r"^jsi18n/$", javascript_catalog, js_info_dict, name="catalog"),

)



handler404 = views.Error404.as_view()
handler403 = views.Error403.as_view()
handler500 = views.Error500.as_view()


if settings.DEBUG:

    docroot = os.path.join(os.path.dirname(__file__), "..", "static")

    urlpatterns += patterns('',
                            url(r"^static/(?P<path>.*)$",
                                "django.views.static.serve",
                                {"document_root": docroot}))