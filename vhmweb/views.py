from django.conf import settings
from django.core.urlresolvers import reverse
from django.http.response import HttpResponseRedirect, Http404
from django.shortcuts import render_to_response
from django.utils.translation import ugettext_lazy as _, ugettext
from django.views.generic.base import TemplateView, RedirectView
from django.views.generic.edit import FormView
from vhmdb.models.rent import VhostRent
from vhmdb.models.user import User
from vhmdb.models.vhost import Vhost
from vhmmisc.auth import login, logout
from vhmmisc.forms import LoginForm
from vhmmisc.requests import get_request
from vhmmisc.views import LoginMixin, TitleMixin, AngularMixin, SettingsMixin, NoCacheMixin


class IndexView(TemplateView, LoginMixin, AngularMixin, SettingsMixin):
    template_name = "vhmweb/base.html"

    def get_ng_app(self):
        user = get_request().vhm_user
        if user.status == User.ADMIN:
            return "vhmAdmin"
        elif user.status == User.TEACHER:
            return "vhmTeacher"
        elif user.status == User.STUDENT:
            return "vhmStudent"

    def get_script_url(self):
        user = get_request().vhm_user
        if user.status == User.ADMIN:
            if settings.DEBUG:
                return "js/admin.js"
            else:
                return "js/admin.min.js"
        elif user.status == User.TEACHER:
            if settings.DEBUG:
                return "js/teacher.js"
            else:
                return "js/teacher.min.js"
        elif user.status == User.STUDENT:
            if settings.DEBUG:
                return "js/student.js"
            else:
                return "js/student.min.js"

    def get_base_url(self):
        user = get_request().vhm_user
        if user.status == User.ADMIN:
            return "/api/admin"
        elif user.status == User.TEACHER:
            return "/api/teacher"
        elif user.status == User.STUDENT:
            return "/api/student"

    def get_ng_constants(self):
        return {
            "static_prefix": settings.STATIC_URL,
            "choices": {
                "user_status": [
                    {"label": ugettext("Student"), "value": User.STUDENT},
                    {"label": ugettext("Teacher"), "value": User.TEACHER},
                    {"label": ugettext("Disabled"), "value": User.DISABLED},
                    {"label": ugettext("Administrator"), "value": User.ADMIN},
                ],
                "rent_status": [
                    {"label": ugettext("Active"), "value": VhostRent.ACTIVE},
                    {"label": ugettext("Passed"), "value": VhostRent.PASSED},
                    {"label": ugettext("Waiting"), "value": VhostRent.WAITING},
                    {"label": ugettext("Pending"), "value": VhostRent.PENDING},
                    {"label": ugettext("Rejected"), "value": VhostRent.REJECTED},
                ]
            },
            "datetime_format": settings.DATETIME_JAVASCRIPT_FORMAT,
            "baseUrl": self.get_base_url(),
            "language": settings.LANGUAGE_CODE,
        }


class LoginView(FormView, TitleMixin):

    title = _("Login")

    template_name = "vhmweb/login.html"

    form_class = LoginForm

    success_url = "/"

    def get(self, request, *args, **kwargs):

        if request.vhm_user:
            return HttpResponseRedirect(reverse("vhmweb_index"))

        return super(LoginView, self).get(request, *args, **kwargs)

    def form_valid(self, form):

        self.success_url = form.cleaned_data["next"]

        login(get_request(), User.objects.get(name=form.cleaned_data["username"]))

        return super(LoginView, self).form_valid(form)


class LogoutView(RedirectView, NoCacheMixin):

    def get_redirect_url(self, **kwargs):
        return reverse("vhmmisc_login")

    def get(self, request, *args, **kwargs):

        logout(request)

        return super(LogoutView, self).get(request, *args, **kwargs)


class VhostRedirectView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):

        vhost = kwargs.get("vhost")
        query = None

        try:
            vhost = int(vhost)
            query = {"pk": vhost}
        except ValueError:
            query = {"name": vhost}
        finally:
            try:
                vhost = Vhost.objects.get(**query)
            except Vhost.DoesNotExist:
                return Http404

        host = self.request.get_host()
        if host:
            host = host.split(":")[0]
        url = "http://%s:%s" % (host, vhost.port)
        return url


class ErrorBase(TemplateView, TitleMixin):

    title = _("An error occurred")
    description = _("An unknown error has occurred.")
    code = None

    template_name = "vhmweb/error.html"

    def get_context_data(self, **kwargs):
        data = super(ErrorBase, self).get_context_data(**kwargs)
        data["description"] = self.description
        data["code"] = self.code
        return data

    def get(self, request, *args, **kwargs):
        return render_to_response(self.template_name,self.get_context_data())


class Error404(ErrorBase):
    title = _("Page not found")
    description = _("The page you requested cannot be found.")
    code = 404


class Error403(ErrorBase):
    title = _("Access forbidden")
    description = _("You do not have permissions to view this page.")
    code = 403


class Error500(ErrorBase):
    title = _("Server error")
    description = _("A server error has occurred.")
    code = 500