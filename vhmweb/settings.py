###
# Add your own configurations in this file, to keep original settings intact.
# Remember to especially configure your database and SECRET_KEY. See examples from the settings file.
###

# Don't remove this line!
from vhmweb.default_settings import *

LANGUAGE_CODE = "fi-FI"
TIME_ZONE = "Europe/Helsinki"
DATETIME_JAVASCRIPT_FORMAT = "dd.MM.yyyy HH:mm"